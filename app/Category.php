<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories_multi';
    protected  $primaryKey = 'cat_id';
    protected $appends = ['thumb'];


    public function getThumbAttribute(){
        $json = $this->attributes['cat_picture'];
        $arr = json_decode($json,true);
        return '/assets/upload/full/'.$arr[0]['name'];
    }
}
