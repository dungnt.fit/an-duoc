<?php

namespace App\Providers;

use App\Category;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
//        view()->composer('layouts/header',function($view){
////            $option = Options::all();
////            // dd($option);
////            $socical = Socical::all();
////            $logo = Logo::first();
//            $categories = Category::select('cat_id','cat_name','cat_rewrite','cat_parent_id','cat_has_child','cat_all_child')->where('cat_active',1)->get();
//            $cat_parent = $categories->groupBy('cat_parent_id');
//
//            $view->with(['cat_parent'=>$cat_parent]);
//        });

    }
}
