<?php

namespace App\Http\Controllers;

use App\Category;
use App\Products;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    function showCategories($slug){
        $cat_parent = $this->getCategory();
        $cat = Category::where('cat_rewrite',$slug)->first();
        if(!isset($cat)) return 'not founds';
        if($cat->cat_parent_id==0){
            $arr_pro_id = $cat_parent[$cat->cat_id]->map(function($item){
                return $item->cat_id;
            });
        }else{
            $arr_pro_id = [$cat->cat_id];
        }
        $pro = Products::whereIn('pro_cat_id',$arr_pro_id)->orderBy('pro_id',"DESC")->paginate(30);
        return view('/pages/category',compact('pro','cat','cat_parent'));
    }
}
