<?php

namespace App\Http\Controllers;

use App\Category;
use App\News;
use App\Products;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    public function showNews(){
        $cat_parent = $this->getCategory();
        $news = $this->getNews();
        $cat_news = Category::select('cat_type','cat_rewrite','cat_name','cat_id')->where('cat_type','news')->orderBy('cat_id')->get();
        if (isset($cat_news)){
            foreach ($cat_news as $key=>$category_news){
                $news = News::select('new_category','new_teaser','new_id','new_category','new_title','new_picture','new_hot','new_rewrite','new_description')->where('new_category',$category_news->cat_id)->orderBy('new_id','desc')->limit(4)->get();
                $cat_news[$key]->news = $news->toArray();
                $cat_news[$key]->new_category = isset($cat_news[$category_news->cat_id])?$cat_parent[$category_news->cat_id]:[];
            }
        }
        $pro_hot = Products::select('pro_name','pro_picture','pro_hot','pro_price','pro_id','pro_link')->where('pro_hot',1)->orderBy('pro_id','desc')->limit(15)->get();
        $news_hot = News::select('new_teaser','new_id','new_title','new_hot','new_rewrite','new_picture')->where('new_hot',1)->inrandomOrder()->first();
        return view('pages/news',compact('cat_parent','news','news_hot','cat_news','pro_hot'));
    }
}
