<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductsController extends Controller
{
    public function showDetailProducts($pro_rewrite){
        $pro1 = Products::where('pro_rewrite',$pro_rewrite)->firstOrFail();
        $pro = Products::where('pro_cat_id',$pro1->pro_id)->get();
        return view('/pages/product',compact('pro','cat'));
    }
}
