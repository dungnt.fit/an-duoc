<?php

namespace App\Http\Controllers;

use App\Products;
use Illuminate\Http\Request;

class ProductDetailController extends Controller
{
    function showProductsDetail($pro_rewrite,$pro_id){
        $cat_parent = $this->getCategory();
        $pro_detail = $this->getProductDetail($pro_rewrite,$pro_id);
        $pro_similar = Products::select('pro_cat_root_id','pro_rewrite','pro_name','pro_link','pro_picture','pro_price')->where('pro_cat_root_id',$pro_detail->pro_cat_root_id)->limit(10)->get();
        return view('pages.productdetail',compact('pro_detail','cat_parent','pro_similar'));
    }
}
