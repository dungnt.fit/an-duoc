<?php

namespace App\Http\Controllers;

use App\Category;
use App\News;
use App\Products;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    function getCategory($type=null){
        $categories = Category::select('cat_id','cat_name','cat_rewrite','cat_parent_id','cat_has_child','cat_all_child','cat_picture')->where('cat_active',1)->limit(53)->get();
        $cat_parent = $categories->groupBy('cat_parent_id');
        if(isset($cat_parent[0])){
            if($type==null){
                foreach ($cat_parent[0] as $key=>$category){
                    $cat_parent[0][$key]->categories_child = isset($cat_parent[$category->cat_id])?$cat_parent[$category->cat_id]:[];
                }
            }else if($type=='home'){
                foreach ($cat_parent[0] as $key=>$category){
                    $products = Products::select('pro_id','pro_name','pro_picture','pro_rewrite','pro_cat_root_id')->where('pro_cat_root_id',$category->cat_id)->orderBy('pro_id','desc')->limit(6)->get();
                    $cat_parent[0][$key]->products = $products->toArray();
                    $cat_parent[0][$key]->categories_child = isset($cat_parent[$category->cat_id])?$cat_parent[$category->cat_id]:[];
                }
            }
        }
        return $cat_parent;
    }
    function getProductDetail($pro_rewrite,$pro_id){
        $pro_detail = Products::select('pro_cat_root_id','pro_id','pro_name','pro_rewrite','pro_picture','pro_price','pro_old_price','pro_total_reviews','pro_quantity','pro_description')->find($pro_id);
        return $pro_detail;
    }
    function getNews(){
        $news = News::select('new_teaser','new_id','new_category','new_title','new_picture','new_hot','new_rewrite','new_description')->orderBy('new_id','desc')->limit(4)->get();
        return $news;
    }
}
