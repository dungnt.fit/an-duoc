<?php

namespace App\Http\Controllers;

use App\Category;
use App\Products;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class IndexController extends Controller
{
    public function showIndex(){
        $products_total = Products::orderby('pro_total_reviews','DESC')->limit(5)->get();
        $cat_parent = $this->getCategory('home');
        $pro_cat = Products::orderby('pro_rating','DESC')->get();
        $type_page = 123;
        return view('pages/home',compact('products_total','cat_parent','pro_cat','type_page'));

    }






}
