<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $table = 'products';
    protected  $primaryKey = 'pro_id';

    protected $appends = ['thumb'];


    public function getThumbAttribute(){
        $json = $this->attributes['pro_picture'];
        $arr = json_decode($json,true);
        return '/assets/upload/full/'.$arr[0]['name'];
    }
}
