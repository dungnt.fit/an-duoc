<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/','IndexController@showIndex');
Route::get('/news','NewsController@showNews');
Route::get('/{pro_rewrite}-p{pro_id}.html','ProductDetailController@showProductsDetail')->where([
    'pro_rewrite'=>'[^/]*',
    'pro_id'=>'[0-9]*'
]);
Route::get("/{news_rewrite}-news{news_id}.html",'NewsDetailController@showNewsDetail')->where([
    'news_rewrite'=>'[^/]*',
    'news_id'=>'[0-9]*'
]);
Route::get('/{slug}',"CategoriesController@showCategories");
