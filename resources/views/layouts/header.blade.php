
<header id="header">
    <div class="wrap-header">
        <div id="top-header">
            <h1> Thực phẩm không già</h1>

            <div class="g1180">
                <div id="header-hotline">
                    <p>
                        <i class="mcon-phone-1"></i>
                        Hot Line: 18008183 - 0942080080            </p>
                </div>
                <div id="account-header">
                    <div class="account support">
                        <a href="/" rel="nofollow">
                            Hộ trợ và giải đáp                    <i class="mcon-caret-down"></i>
                        </a>
                        <div class="box-acc-support">
                            <a href="#" rel="nofollow">Giới thiệu</a>
                            <a href="#" rel="nofollow">Tài khoản của tôi</a>
                            <a href="#" rel="nofollow">Hướng dẫn đổi trả và bảo hành</a>
                            <a href="#" rel="nofollow">Hướng dẫn mua hàng</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="fix-scr fix">
            <div id="search-header">
                <div class="g1180">
                    <div class="block-logo">
                        <a href="/" rel="nofollow" class="logo-checking">
                            <img src="/assets/logo/store_1496111869_993.png" alt="Dược phâm khong gia">
                        </a>
                    </div>
                    <div class="block-search">
                        <div class="action-top">
                            <div class="notification parent-tip">
                                <a href="#">
                                    <!--<i class="icon-ring" data-noti="1"></i>-->
                                    <i class="mcon-bell" data-noti="1"></i>
                                    <div>Thông báo</div>
                                </a>
                                <div class="tooltip">
                                    <div class="content-tip content-tip-notification">
                                        <div class="title-tooltip">
                                            <strong>Thông báo</strong>
                                        </div>
                                        <ul>
                                            <li>
                                                <a href="#">
                                                    <div class="noti-img">
                                                        <img />
                                                    </div>
                                                    <div class="noti-content">
                                                        <div class="noti-teaser">
                                                            <b>Võ Viết Hiểu </b> <pan>Đăng tin</pan><b> Bán kho, nhà xưởng  tại Xã Điện Nam </b><span>: Bán Ki Ốt và Shophouse khu phố chợ Điện Nam Bắc gần KCN Điện Nam Điện Ngọc giá gốc Chủ đầu tư </span>
                                                            <span>: Bán Ki Ốt và Shophouse khu phố chợ Điện Nam Bắc gần KCN Điện Nam Điện Ngọc giá gốc Chủ đầu tư </span>
                                                        </div>
                                                        <time>11/12/2018</time>
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <div class="noti-img">
                                                        <img />
                                                    </div>
                                                    <div class="noti-content">
                                                        <div class="noti-teaser">
                                                            <b>Võ Viết Hiểu </b> <pan>Đăng tin</pan><b> Bán kho, nhà xưởng  tại Xã Điện Nam </b><span>: Bán Ki Ốt và Shophouse khu phố chợ Điện Nam Bắc gần KCN Điện Nam Điện Ngọc giá gốc Chủ đầu tư </span>
                                                            <span>: Bán Ki Ốt và Shophouse khu phố chợ Điện Nam Bắc gần KCN Điện Nam Điện Ngọc giá gốc Chủ đầu tư </span>
                                                        </div>
                                                        <time>11/12/2018</time>
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <div class="noti-img">
                                                        <img />
                                                    </div>
                                                    <div class="noti-content">
                                                        <div class="noti-teaser">
                                                            <b>Võ Viết Hiểu </b> <pan>Đăng tin</pan><b> Bán kho, nhà xưởng  tại Xã Điện Nam </b><span>: Bán Ki Ốt và Shophouse khu phố chợ Điện Nam Bắc gần KCN Điện Nam Điện Ngọc giá gốc Chủ đầu tư </span>
                                                            <span>: Bán Ki Ốt và Shophouse khu phố chợ Điện Nam Bắc gần KCN Điện Nam Điện Ngọc giá gốc Chủ đầu tư </span>
                                                        </div>
                                                        <time>11/12/2018</time>
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <div class="noti-img">
                                                        <img />
                                                    </div>
                                                    <div class="noti-content">
                                                        <div class="noti-teaser">
                                                            <b>Võ Viết Hiểu </b> <pan>Đăng tin</pan><b> Bán kho, nhà xưởng  tại Xã Điện Nam </b><span>: Bán Ki Ốt và Shophouse khu phố chợ Điện Nam Bắc gần KCN Điện Nam Điện Ngọc giá gốc Chủ đầu tư </span>
                                                            <span>: Bán Ki Ốt và Shophouse khu phố chợ Điện Nam Bắc gần KCN Điện Nam Điện Ngọc giá gốc Chủ đầu tư </span>
                                                        </div>
                                                        <time>11/12/2018</time>
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <div class="noti-img">
                                                        <img />
                                                    </div>
                                                    <div class="noti-content">
                                                        <div class="noti-teaser">
                                                            <b>Võ Viết Hiểu </b> <pan>Đăng tin</pan><b> Bán kho, nhà xưởng  tại Xã Điện Nam </b><span>: Bán Ki Ốt và Shophouse khu phố chợ Điện Nam Bắc gần KCN Điện Nam Điện Ngọc giá gốc Chủ đầu tư </span>
                                                            <span>: Bán Ki Ốt và Shophouse khu phố chợ Điện Nam Bắc gần KCN Điện Nam Điện Ngọc giá gốc Chủ đầu tư </span>
                                                        </div>
                                                        <time>11/12/2018</time>
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <div class="noti-img">
                                                        <img />
                                                    </div>
                                                    <div class="noti-content">
                                                        <div class="noti-teaser">
                                                            <b>Võ Viết Hiểu </b> <pan>Đăng tin</pan><b> Bán kho, nhà xưởng  tại Xã Điện Nam </b><span>: Bán Ki Ốt và Shophouse khu phố chợ Điện Nam Bắc gần KCN Điện Nam Điện Ngọc giá gốc Chủ đầu tư </span>
                                                            <span>: Bán Ki Ốt và Shophouse khu phố chợ Điện Nam Bắc gần KCN Điện Nam Điện Ngọc giá gốc Chủ đầu tư </span>
                                                        </div>
                                                        <time>11/12/2018</time>
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <div class="noti-img">
                                                        <img />
                                                    </div>
                                                    <div class="noti-content">
                                                        <div class="noti-teaser">
                                                            <b>Võ Viết Hiểu </b> <pan>Đăng tin</pan><b> Bán kho, nhà xưởng  tại Xã Điện Nam </b><span>: Bán Ki Ốt và Shophouse khu phố chợ Điện Nam Bắc gần KCN Điện Nam Điện Ngọc giá gốc Chủ đầu tư </span>
                                                            <span>: Bán Ki Ốt và Shophouse khu phố chợ Điện Nam Bắc gần KCN Điện Nam Điện Ngọc giá gốc Chủ đầu tư </span>
                                                        </div>
                                                        <time>11/12/2018</time>
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <div class="noti-img">
                                                        <img />
                                                    </div>
                                                    <div class="noti-content">
                                                        <div class="noti-teaser">
                                                            <b>Võ Viết Hiểu </b> <pan>Đăng tin</pan><b> Bán kho, nhà xưởng  tại Xã Điện Nam </b><span>: Bán Ki Ốt và Shophouse khu phố chợ Điện Nam Bắc gần KCN Điện Nam Điện Ngọc giá gốc Chủ đầu tư </span>
                                                            <span>: Bán Ki Ốt và Shophouse khu phố chợ Điện Nam Bắc gần KCN Điện Nam Điện Ngọc giá gốc Chủ đầu tư </span>
                                                        </div>
                                                        <time>11/12/2018</time>
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <div class="noti-img">
                                                        <img />
                                                    </div>
                                                    <div class="noti-content">
                                                        <div class="noti-teaser">
                                                            <b>Võ Viết Hiểu </b> <pan>Đăng tin</pan><b> Bán kho, nhà xưởng  tại Xã Điện Nam </b><span>: Bán Ki Ốt và Shophouse khu phố chợ Điện Nam Bắc gần KCN Điện Nam Điện Ngọc giá gốc Chủ đầu tư </span>
                                                            <span>: Bán Ki Ốt và Shophouse khu phố chợ Điện Nam Bắc gần KCN Điện Nam Điện Ngọc giá gốc Chủ đầu tư </span>
                                                        </div>
                                                        <time>11/12/2018</time>
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <div class="noti-img">
                                                        <img />
                                                    </div>
                                                    <div class="noti-content">
                                                        <div class="noti-teaser">
                                                            <b>Võ Viết Hiểu </b> <pan>Đăng tin</pan><b> Bán kho, nhà xưởng  tại Xã Điện Nam </b><span>: Bán Ki Ốt và Shophouse khu phố chợ Điện Nam Bắc gần KCN Điện Nam Điện Ngọc giá gốc Chủ đầu tư </span>
                                                            <span>: Bán Ki Ốt và Shophouse khu phố chợ Điện Nam Bắc gần KCN Điện Nam Điện Ngọc giá gốc Chủ đầu tư </span>
                                                        </div>
                                                        <time>11/12/2018</time>
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <div class="noti-img">
                                                        <img />
                                                    </div>
                                                    <div class="noti-content">
                                                        <div class="noti-teaser">
                                                            <b>Võ Viết Hiểu </b> <pan>Đăng tin</pan><b> Bán kho, nhà xưởng  tại Xã Điện Nam </b><span>: Bán Ki Ốt và Shophouse khu phố chợ Điện Nam Bắc gần KCN Điện Nam Điện Ngọc giá gốc Chủ đầu tư </span>
                                                            <span>: Bán Ki Ốt và Shophouse khu phố chợ Điện Nam Bắc gần KCN Điện Nam Điện Ngọc giá gốc Chủ đầu tư </span>
                                                        </div>
                                                        <time>11/12/2018</time>
                                                    </div>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="user-head parent-tip">
                                <a href="/dang-nhap.aspx?urlreturn=Lw--">
                                    <i class="mcon-user-1"></i>
                                    <div>
                                        <span>Tài khoản</span>
                                        <span>Đăng nhập</span>
                                    </div>
                                </a>

                            </div>
                            <div class="cart">

                                <a href="v3_list-cart-page">

                                    <i class="mcon-shopping-cart-3"></i>
                                    <span class="title-cart">Giỏ hàng</span>

                                    <span class="num-in-cart" value="0" id="num-in-cart">0</span>
                                </a>







                            </div>
                            <!--                    -->                </div>
                        <form action="/search" id="search" method="GET">
                            <input name="keyword" type="text" placeholder="Tìm kiếm sản phẩm tại đây" value="">
                            <button class="search-submit"><i class="mcon-search-bold"></i></button>
                            <a href="/profile/dang-ban.aspx" class="btn-upload-product"><i class="mcon-cloud-upload"></i>đăng bán</a>
                        </form>
                        <div class="suggest">
                            <p><b>Từ khóa phổ biến</b></p>
                            <div class="suggest-list">
                                <p>Giảm cân New Perfect, Rich Slim, Tăng cường sinh lý nam Ngọc Để Hoàn,</p>
                            </div>
                            <p class="clear-fix"></p>
                        </div>
                    </div>
                    <div class="clear-fix"></div>
                </div>
            </div>
        </div>
        <div class="clear-fix"></div>
            <div id="header-res" class="g1180">
                <div class="fa-menu">
                    <a class="btn-menu" href="">
                        <i class="mcon-bars"></i>
                    </a>
                    <div class="menu-res">
                        <h4 class="title-menu">
                            <a href="/" title="Logo anduoc.vn">
                                <img src="/assets/logo/store_1496111869_993.png" alt="Logo anduoc.vn">
                            </a>
                        </h4>
                        <ul class="list-menu-res">
                            <li class="item-menu"><a href="/"> Trang chủ</a></li>
                            <li class="item-menu"><a href="/news"> Tin tức - sự kiện</a></li>
                            @if(isset($cat_parent[0]))
                                @foreach($cat_parent[0] as $cat_item)
                                    <li class="item-menu"><a href="/{{$cat_item->cat_rewrite}}" title="{{$cat_item->cat_name}}">{{$cat_item->cat_name}}</a><i class="mcon-angle-right"></i>
                                        <div class="content-sub-menu">
                                            <h4 title="{{$cat_item->cat_name}}" class="title-menu"><i class="mcon-angle-left" aria-hidden="true"></i> Thực phẩm chức năng</h4>
                                            <ul class="sub-menu">
                                                @if(isset($cat_parent[$cat_item->cat_id]))
                                                    @foreach($cat_parent[$cat_item->cat_id] as $item_child)
                                                        <li class="item-menu"><a title="{{$item_child->cat_name}}" href="/{{$item_child->cat_rewrite}}">{{$item_child->cat_name}}</a></li>
                                                    @endforeach
                                                @endif
                                            </ul>
                                        </div>
                                    </li>
                                @endforeach
                            @endif
                        </ul>
                    </div>

                </div>

                <div class="fas-search">
                    <div class="btn-search-res">
                        <form action="/search" class="form-search-res" method="get">
                            <input name="keyword" type="text" placeholder="Tìm kiếm" class="text-search-res">
                        </form>
                        <i class="mcon-search"></i>
                    </div>
                </div>
                <div class="fa-cart">
                    <a href="list-cart-page">
                        <i class="mcon-shopping-cart-2"></i>
                    </a>
                    <span id="num-in-cart-mobile">0</span>
                </div>
                <div class="acc-res">
                    <div class="btn-acc-res">
                        <i class="mcon-user"></i>
                    </div>
                    <div class="content-acc-ress">
                        <h4 class="title-menu">
                            menu
                        </h4>
                        <ul class="list-acc-res">
                            <li class="item-menu"  ><a href="list-cart-page" id="num-in-cart-1"> Giỏ hàng (<b >0</b>)</a></li>
                            <li class="item-menu">

                            </li>
                            <li><a href="/dang-nhap.aspx?urlreturn=Lw--"><img src="/assets/images/icon-login-facebook.png" width="100%"/> </a></li>

                            <li class="item-menu">
                                <a href="">Tài khoản</a>
                            </li>
                            <li class="item-menu">
                                <a href="">Yêu thích</a>
                            </li>
                            <li class="item-menu">
                                <a href="">Đã xem</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="main-menu-fix">
                <div id="list-menu" class="g1180">
                    <div id="slimScrollDiv">
                        <nav id="menu" class="menu">
                            <p class="cate-menu">
                                <i class="mcon-bars"></i>
                                <span>Danh mục sản phẩm</span>
                            </p>
                            <ul
                            <?php
                                if (isset($type_page)) {
                                    echo 'style= "display: block;padding: 7px 0px;margin-top: 10px;" ';
                                }
                                ?>>
                                @if(isset($cat_parent[0]))
                                    @foreach($cat_parent[0] as $cat_item)
                                        <li>
                                            <a title="{{$cat_item->cat_name}}" href="/{{$cat_item->cat_rewrite}}"> <i class="mcon-angle-right" aria-hidden="true"></i> {{$cat_item->cat_name}}</a>
                                            <ul class="sub-menu">
                                                <p class="title-sub-menu"><a href="/{{$cat_item->cat_rewrite}}" title="{{$cat_item->cat_name}}">{{$cat_item->cat_name}}</a></p>
                                                @if(isset($cat_parent[$cat_item->cat_id]))
                                                    @foreach($cat_parent[$cat_item->cat_id] as $item_child)
                                                        <li class=""><a href="/{{$item_child->cat_rewrite}}" title="{{$item_child->cat_name}}">{{$item_child->cat_name}}</a></li>
                                                    @endforeach
                                                @endif
                                            </ul>
                                        </li>
                                        <li class="news-active">
                                        </li>
                                    @endforeach
                                @endif
                            </ul>
                        </nav>
                    </div>

                    <div id="policy-header">
                        <a href="/news">Tin tức - sự kiện</a>
                        <a href="" class="si nth1"><i class="mcon-cart-shiping"></i> Giao hàng toàn quốc</a>
                        <a href="" class="si nth3"><i class="mcon-shipping-timed"></i> Đổi trả trong 10 ngày</a>
                    </div>
                </div>
            </div>
    </div>

</header>