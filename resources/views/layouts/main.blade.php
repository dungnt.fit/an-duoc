
<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <title>Dược phẩm An dược</title>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="keywords" content="Dược phẩm An dược">
    <meta name="description" content="Dược phẩm An dược">
    <meta property="og:title" content="Dược phẩm An dược">
    <meta property="og:description" content="Dược phẩm An dược"/>
    <meta property="og:url" content="http://dev.anduoc.vn">
    <meta property="og:type" content="product">
    <meta property="og:image" content="http://dev.anduoc.vn/assets/images/store_1496111869_994.png">
    <meta name="DC.language" content="scheme=utf-8 content=vi">
    <meta name="google-site-verification" content="meta_google_site_verification">
    <link href="/assets/images/store_1496111869_869.png"  alt="logo" rel="shortcut icon" type="image/vnd.microsoft.icon">
    <link rel="stylesheet" href="/assets/css/css_all.css?v=970">

    <script src="/component/js/jquery-3.2.1.min.js"></script>
    <script src="/component/js/slick.min.js"></script>
    <script type="text/javascript" src="/assets/js/config.js?v=5208"></script>
    <script type="text/javascript" src="/assets/js/consult.js?v=884"></script>
    <script type="text/javascript" src="/assets/js/animate.js?v=919"></script>

</head>
<body class="home-anduoc">
@include('layouts/header')

@yield('content')

@include('/layouts/footer')

<script>
    $(document).ready(function () {
        lazyLoadImg();
    });
</script>

</body>
</html>
