<footer>
    <!-- Begin footer -->

    <div class="footer-border">
        <div id="boder-footer">
            <div class="inner-boder g1180">
                    <span>
                    </span>
            </div>
        </div>
        <div class="footer-content">
            <div class="footer-content-top">
                <div class="g1180">
                    <div class="w25 fl box-footer-left">
                        <div id="logo-footer"><img src="/assets/logo/store_1496111869_993.png" alt="Dược phâm khong gia"></div>
                        <h3 class="company"> An Dược</h3>
                        <p class="address-footer">102 Thái Thịnh, Quận Đống Đa, TP. Hà Nội</p>
                    </div>

                    <div class="w75 fl box-footer-right">
                        <ul>
                            <li>

                                <h6>
                                    <a href="thuc-pham-chuc-nang"><span>Thực phẩm chức năng</span></a>
                                </h6>
                                <a href="tpcn-giam-can"
                                   title="TPCN - Giảm cân">
                                    <span>TPCN - Giảm cân</span></a>
                                <a href="tpcn-no-nguc"
                                   title="TPCN - Nở Ngực">
                                    <span>TPCN - Nở Ngực</span></a>
                                <a href="tpcn-collagen"
                                   title="TPCN - Collagen">
                                    <span>TPCN - Collagen</span></a>
                                <a href="tpcn-glucosamine-canxi"
                                   title="TPCN - Glucosamine, Canxi">
                                    <span>TPCN - Glucosamine, Canxi</span></a>
                                <a href="tpcn-linh-chi"
                                   title="TPCN - Linh Chi">
                                    <span>TPCN - Linh Chi</span></a>
                            </li>
                            <li>

                                <h6>
                                    <a href="my-pham-lam-dep"><span>Mỹ phẩm làm đẹp</span></a>
                                </h6>
                                <a href="my-pham-lam-dep-da"
                                   title="Mỹ Phẩm Làm Đẹp Da">
                                    <span>Mỹ Phẩm Làm Đẹp Da</span></a>
                                <a href="my-pham-trang-diem"
                                   title="Mỹ Phẩm Trang Điểm">
                                    <span>Mỹ Phẩm Trang Điểm</span></a>
                                <a href="cham-soc-toc"
                                   title="Chăm Sóc Tóc">
                                    <span>Chăm Sóc Tóc</span></a>
                            </li>
                            <li>

                                <h6>
                                    <a href="suc-khoe-tinh-duc"><span>Sức khỏe tình dục</span></a>
                                </h6>
                                <a href="ngua-thai"
                                   title="Ngừa thai">
                                    <span>Ngừa thai</span></a>
                                <a href="tang-cuong-sinh-ly"
                                   title="Tăng Cường Sinh Lý">
                                    <span>Tăng Cường Sinh Lý</span></a>
                                <a href="boi-tron"
                                   title="Bôi trơn">
                                    <span>Bôi trơn</span></a>
                            </li>
                            <li>

                                <h6>
                                    <a href="nhan-sam-hong-sam"><span>Nhân sâm hồng sâm</span></a>
                                </h6>
                                <a href="nhan-sam"
                                   title="Nhân sâm">
                                    <span>Nhân sâm</span></a>
                                <a href="hong-sam"
                                   title="Hồng sâm">
                                    <span>Hồng sâm</span></a>
                            </li>
                            <li>

                                <h6>
                                    <a href="dung-cu-y-te"><span>Dụng cụ y tế</span></a>
                                </h6>
                                <a href="cham-soc-chan-thuong-so-cuu"
                                   title="Chăm Sóc Chấn Thương - Sơ Cứu">
                                    <span>Chăm Sóc Chấn Thương - Sơ Cứu</span></a>
                                <a href="nhiet-ke"
                                   title="Nhiệt Kế">
                                    <span>Nhiệt Kế</span></a>
                                <a href="may-do-duong-huyet"
                                   title="Máy Đo Đường Huyết">
                                    <span>Máy Đo Đường Huyết</span></a>
                                <a href="may-do-huyet-ap"
                                   title="Máy Đo Huyết Áp">
                                    <span>Máy Đo Huyết Áp</span></a>
                                <a href="can-suc-khoe"
                                   title="Cân Sức Khỏe">
                                    <span>Cân Sức Khỏe</span></a>
                                <a href="cac-loai-may-y-te-khac"
                                   title="Các Loại Máy Y Tế Khác">
                                    <span>Các Loại Máy Y Tế Khác</span></a>
                            </li>

                        </ul>
                    </div>

                </div>


            </div>
        </div>
        <div class="footer-content-bottom">
            <div class="g1180"> <span>© Copyright 2008-2018</span> </div>

        </div>


    </div>
    <!--Begin Menu Right-->
    <div id="apendMenuRight">
        <ul id="menuRight">
            <li class="drag-cart">
                <a href="">
                    <p>
                        <i class="mcon-shopping-cart-2"></i>
                        Giỏ hàng                        <span class="change-I-cart">0</span>
                    </p>
                </a>
            </li>
            <li class="taikhoan">
                <a href="">
                    <p>
                        <i class="mcon-user-2"></i>
                        Tài khoản                    </p>
                </a>
            </li>
            <li class="yeuthich">
                <a href="">
                    <p>
                        <i class="mcon-heart"></i>
                        Yêu thích                    </p>
                </a>
            </li>
            <li class="daxem">
                <a href="">
                    <p>
                        <i class="mcon-eye"></i>
                        Đã xem                    </p>
                </a>
            </li>
            <li class="scrollTop">
                <p><i class="si"></i></p>
            </li>
        </ul>
    </div>
    <!--End Menu Right-->

    <div class="scroll-top">
        <i class="mcon-angle-up"></i>
    </div>

    <div id="call-me">
        <a href="tel:18008183" class="btn-call-now"><i class="mcon-phone-square"></i>Gọi miễn phí</a>
    </div>

    <!-- End footer -->

    <!--
            <link rel="stylesheet" href="/assets/css/style_config.css?v=">
            --></footer>