@extends('layouts/main')
@section('content')
    <?php
    $a = (int)$pro_detail->pro_price;
    $b = (int)$pro_detail->pro_old_price;
    if($pro_detail->pro_old_price!=0){
        $promotion = 100-(($a/$b)*100);
    }else
        $promotion=0;

    ?>
    <section id="content" class="content">
        <div class="inner-content g1180">

            <div class="wrap-content w100 fl">
                <div class="over-lay">

                </div>
                {{--<div class="pop-up">--}}
                    {{--<div class="box_product_incart util-clearfix">--}}

                        {{--<p class="name">--}}
                            {{--<img src="assets/images/tick.png">--}}


                        {{--</p>--}}
                        {{--<strong>--}}
                            {{--Thêm sản phẩm thành công!--}}
                        {{--</strong>--}}
                    {{--</div>--}}

                {{--</div>--}}
                <div class="breadcrumbs">
                    <ol class="breadcrumb-page " itemscope="" itemtype="http://schema.org/BreadcrumbList"><li class="item-breadcrumb" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><a itemprop="item" title="Trang chủ" href="http://dev.anduoc.vn"><span itemprop="name">Trang chủ</span></a><meta itemprop="position" content="1"></li> <li class="item-breadcrumb" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><a itemprop="item" title="Thực phẩm đồ uống" href="http://dev.anduoc.vn/thuc-pham-do-uong"><span itemprop="name">Thực phẩm đồ uống</span></a><meta itemprop="position" content="2"></li> <li class="item-breadcrumb" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><a itemprop="item" title="Nước Giải Khát" href="http://dev.anduoc.vn/nuoc-giai-khat"><span itemprop="name">Nước Giải Khát</span></a><meta itemprop="position" content="3"></li> <li class="item-breadcrumb" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><a itemprop="item" title="Nước ép hỗn hợp Rau và trái cây tổng hợp Tipco hộp 1 L" href="http://dev.anduoc.vn/nuoc-ep-hon-hop-rau-va-trai-cay-tong-hop-tipco-hop-1-l-p772.html"><span itemprop="name">Nước ép hỗn hợp Rau và trái cây tổng hợp Tipco hộp 1 L</span></a><meta itemprop="position" content="4"></li></ol>        </div>
                <div class="introduce-product">
                    <div class="fistProductView">
                        <div class="productImage slick-initialized slick-slider" id="img-pro-full">
                            <div class="slick-list draggable"><div class="slick-track" style="opacity: 1; width: 389px; transform: translate3d(0px, 0px, 0px);"><img alt="{{$pro_detail->pro_name}}" src="{{$pro_detail->thumb}}" class="slick-slide slick-current slick-active" data-slick-index="0" aria-hidden="false" style="width: 389px;" tabindex="0"></div></div>
                        </div>
                        <div class="productImageZoomItem slick-initialized slick-slider" id="img-pro-thumb">
                            <div class="slick-list draggable" style="padding: 0px 50px;"><div class="slick-track" style="opacity: 1; width: 97px; transform: translate3d(97px, 0px, 0px);"><img alt="{{$pro_detail->pro_name}}" src="{{$pro_detail->thumb}}" onerror="img_error(this)" class="slick-slide slick-current slick-center" data-slick-index="0" aria-hidden="true" style="width: 80px;" tabindex="0"></div></div>
                        </div>
                    </div>
                    <div class="middleProductView">
                        <div class="productRate">
                            <h1 title="{{$pro_detail->pro_name}}">{{$pro_detail->pro_name}}</h1>
                        </div>
                        <div class="BillInformation">
                            <p class="price-pro">{{number_format($pro_detail->pro_price)}} VNĐ</p>
                            <p>Giá trước đây<span style="text-decoration: line-through">{{number_format($pro_detail->pro_old_price)}} VNĐ</span>
                            </p>
                            <ul class="vote-view">
                                <li>
                                    <div class="rating_user">
                                        <span class="star-view">
                                                    <i class="mcon-star"></i>
                                                    <i class="mcon-star active"></i>
                                                </span>
                                        <span class="star-view">
                                                    <i class="mcon-star"></i>
                                                    <i class="mcon-star active"></i>
                                                </span>
                                        <span class="star-view">
                                                    <i class="mcon-star"></i>
                                                    <i class="mcon-star active"></i>
                                                </span>
                                        <span class="star-view">
                                                    <i class="mcon-star"></i>
                                                    <i class="mcon-star active"></i>
                                                </span>
                                        <span class="star-view">
                                                    <i class="mcon-star"></i>
                                                    <i class="mcon-star active star-width-20"></i>
                                                </span>
                                    </div>
                                </li>
                                <li>
                                    <span>{{$pro_detail->pro_total_reviews}}</span>
                                    <span>Đánh giá</span>
                                </li>
                                <li>
                                    <span class="large" style="color:#61bc48;">2246</span>
                                    <span>Lượt đã xem</span>
                                </li>
                                <li>
                                    <span class="large large-34">78</span>
                                    <span>Lượt mua thành công</span>
                                </li>
                            </ul>
                            <p class="save-prodetail">Tiết kiệm<b>{{$promotion}}%</b></p>
                            <div class="ProductInformation">
                                <table>
                                    <tbody>
                                    <tr>
                                        <td class="bold">ID sản phẩm</td>
                                        <td>{{$pro_detail->pro_id}}</td>
                                    </tr>
                                    <tr>
                                        <td class="bold">Tên sản phẩm</td>
                                        <td>{{$pro_detail->pro_name}}</td>
                                    </tr>
                                    <tr>
                                        <td class="bold">Thương hiệu</td>
                                        <td>Đang cập nhật</td>
                                    </tr>
                                    <tr>
                                        <td class="bold">Xuất xứ</td>
                                        <td>Đang cập nhật</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="SaleProduct">
                            <div>
                                <div class="pick">
                                    <div class="head">Số lượng</div>
                                    <div class="btn" id="sub">-</div>
                                    <input class="input" type="text" id="quantity" value="1">

                                    <div class="btn" id="add">+</div>
                                    <div class="head head-pro"><span class="pro-quantity">{{$pro_detail->pro_quantity}}</span> sản phẩm có sẵn                            </div>
                                </div>
                                <div class="quantity-error">
                                    <p>Xin lỗi chúng tôi không còn đủ sản phẩm này!</p>
                                </div>
                            </div>
                        </div>
                        <div class="add-to-cart">
                            <form id="consult_form" method="post" action="consult">
                                <div class="product-evaluate-title">
                                    <h3>Đăng ký nhận tư vấn về sản phẩm</h3>
                                </div>
                                <div class="comment-box-right" style="width: 100%">
                                    <input type="hidden" name="pro_id" value="772">
                                    <div id="buy-btn">
                                        <input type="hidden" name="slug" value="/nuoc-ep-hon-hop-rau-va-trai-cay-tong-hop-tipco-hop-1-l-p772.html">
                                        <input type="hidden" name="spam_status" value="2">
                                        <input id="con_name" class="content_reviews" name="name_user" type="text" placeholder="Họ tên">
                                        <input id="con_phone" class="content_reviews" name="phone" type="number" placeholder="Số điện thoại">
                                        <input id="con_email" class="content_reviews" name="email" type="text" placeholder="Email">
                                        <input id="token" type="hidden" name="token">
                                        <button type="submit" id="consult">Đăng ký tư vấn</button>                        </div>
                                </div>
                                <i style="color: red" class="error_consult"></i><br>
                            </form>
                        </div>

                        <div class="support-online">
                            <h2>Hỗ trợ trực tuyến</h2>
                            <p>
                                <i class="mcon-skype blue"></i>
                                <span>Hỗ trợ 1-18008183 </span>
                            </p>
                            <p>
                                <i class="mcon-smile red"></i>
                                <span>Hỗ trợ 2-0944944449 </span>
                            </p>
                        </div>
                    </div>
                    <div class="info-product">
                        <div class="information-result">
                            <div class="information-result-title">
                                <span> Thông tin chi tiết</span>
                            </div>
                            <div class="information-result-content">
                                {!! html_entity_decode($pro_detail->pro_description) !!}
                            </div>
                            <a href="javascript:void(0)" class="view-all-detail" title="Xem thêm sản phẩm" rel="nofollow" onclick="viewAllInfomationContent()">Xem thêm</a>
                        </div>
                        <div class="product-evaluate">
                            <div class="product-evaluate-title">
                                <h3>Đánh giá sản phẩm</h3>
                            </div>

                            <div class="product-evaluate-content">
                                <script>
                                    user.set(0, '', 772, 1, 0, '/dang-nhap.aspx?urlreturn=L251b2MtZXAtaG9uLWhvcC1yYXUtdmEtdHJhaS1jYXktdG9uZy1ob3AtdGlwY28taG9wLTEtbC1wNzcyLmh0bWw-');
                                </script>
                                <div class="result_rating w40">
                                    <div class="wrap_star_reviews w40">
                                        <div class="start_review"><i class="mcon-star"></i><span class="point-review">4.2</span></div>
                                        <div class="text_star_reviews">
                                            Sản phẩm như mô tả                                    <br>
                                            (5 đánh giá)
                                        </div>
                                    </div>
                                    <div class="content_detail_star_reviews w60">
                                        <div class="item_detail_star_reviews">
                                            <span class="number_star">5 <i class="mcon-star"></i></span>
                                            <span class="percent_bar">
                                           <span style="width: 40%"></span>
                                       </span>
                                            <span class="percent_star">40%</span>
                                        </div>
                                        <div class="item_detail_star_reviews">
                                            <span class="number_star">4 <i class="mcon-star"></i></span>
                                            <span class="percent_bar">
                                           <span style="width: 40%"></span>
                                       </span>
                                            <span class="percent_star">40%</span>
                                        </div>
                                        <div class="item_detail_star_reviews">
                                            <span class="number_star">3 <i class="mcon-star"></i></span>
                                            <span class="percent_bar">
                                           <span style="width: 20%"></span>
                                       </span>
                                            <span class="percent_star">20%</span>
                                        </div>
                                        <div class="item_detail_star_reviews">
                                            <span class="number_star">2 <i class="mcon-star"></i></span>
                                            <span class="percent_bar">
                                           <span style="width: 0%"></span>
                                       </span>
                                            <span class="percent_star">0%</span>
                                        </div>
                                        <div class="item_detail_star_reviews">
                                            <span class="number_star">1 <i class="mcon-star"></i></span>
                                            <span class="percent_bar">
                                           <span style="width: 0%"></span>
                                       </span>
                                            <span class="percent_star">0%</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="product-evaluate-box">
                                    <form id="form_reviews" class=" w100">
                                        <div class="message-eval">
                                            <span>Bạn đánh giá sản phẩm này được mấy  <i class="mcon-star" aria-hidden="true"></i></span>
                                        </div>
                                        <!--                                --><!--                                    <div id="login_revivews">-->
                                        <!--                                        <a href="/dang-nhap.aspx?urlreturn=--><!--">--><!----><!--</a>-->
                                        <!--                                    </div>-->
                                        <!--                                    -->
                                        <!--                                <input type="hidden" name="id_user" value="--><!--">-->
                                        <!--                                <input type="hidden" name="name_user" value="--><!--">-->
                                        <input type="hidden" name="id_object" value="772">
                                        <!--                                <input type="hidden" name="rev_type" value="--><!--">-->
                                        <!--                                <input type="hidden" name="fbId_user" value="--><!--">-->
                                        <div class="tab_rev product-evaluate-box-rate">
                                            <p>Đánh giá: </p>

                                            <section id="rating_product" class="rating">
                                                <input type="radio" id="star5" name="rating" value="5">
                                                <label class="mcon" for="star5" title="Tuyệt vời"></label>
                                                <input type="radio" id="star4" name="rating" value="4">
                                                <label class="mcon" for="star4" title="Hài lòng"></label>
                                                <input type="radio" id="star3" name="rating" value="3">
                                                <label class="mcon" for="star3" title="Bình thường"></label>
                                                <input type="radio" id="star2" name="rating" value="2">
                                                <label class="mcon" for="star2" title="Không hài lòng"></label>
                                                <input type="radio" id="star1" name="rating" value="1">
                                                <label class="mcon" for="star1" title="Tồi tệ"></label>
                                            </section>
                                            <p class="status_rating">Mức độ hài lòng!</p>

                                        </div>
                                        <!--                                <div id="SubmitReviews" class="btnLogin">-->
                                        <!---->
                                        <!--                                    <button type="submit">--><!--</button>-->
                                        <!--                                </div>-->
                                        <div class="tab_rev product-evaluate-box-content comment-box">
                                            <!--                                <p> --><!--</p>-->
                                            <div class="comment-box-left">
                                                <textarea id="comment" class="content_reviews" name="content_reviews" spellcheck="false" placeholder="Nhập nội dung đánh giá (tối thiểu 30 ký tự)" style=""></textarea>
                                            </div>
                                            <div class="comment-box-right">
                                                <div>
                                                    <input id="review_name" class="content_reviews" name="name_user" type="text" placeholder="Họ tên">
                                                    <input id="review_phone" class="content_reviews" name="phone" type="number" placeholder="Số điện thoại">
                                                </div>
                                                <div>
                                                    <input id="review_email" class="content_reviews" name="email" type="text" placeholder="Email">
                                                    <input type="hidden" name="slug" value="/nuoc-ep-hon-hop-rau-va-trai-cay-tong-hop-tipco-hop-1-l-p772.html">
                                                    <input type="hidden" name="spam_status" value="2">
                                                    <input type="hidden" id="_token" name="_token">
                                                    <button type="submit">Đăng</button>
                                                </div>
                                            </div>
                                        </div>
                                        <i class="error_reviews"></i>
                                    </form>
                                </div>
                            </div>
                            <div class="product-facebook-comment">
                                <div class="product-facebook-comment-content">
                                    <div id="box-reviews">
                                        <div class="item-reviews" id="reviews-253" data-id="253">
                                            <div class="ava-user-rev" style="background-image: url('http://graph.facebook.com/979376715571929/picture?type=square')"></div>
                                            <div class="reviews_main">
                                                <h4 class="name-user">
                <span class="avat-cm">
                    <div class="avatar-user" style="background-image: url('http://graph.facebook.com/979376715571929/picture?type=square')"></div>
                </span>
                                                    <div class="box-user-rev">
                                                        <a href="http://facebook.com/979376715571929" target="_blank">Lin Beer</a>
                                                        <div class="rating_user">
                                                            <i class="mcon-star active"></i><i class="mcon-star active"></i><i class="mcon-star active"></i><i class="mcon-star active"></i><i class="mcon-star active"></i>                                            <span>11 tháng trước</span>
                                                        </div>
                                                    </div>
                                                </h4>

                                                <p class="content-reviews"><span>assad</span></p>
                                            </div>
                                            <div class="bottom-reviews">
                                                <a class="reply">Trả lời</a>
                                            </div>

                                            <div class="sub-reviews sub-reviews-fix" id="sub-reviews-253">
                                            </div>
                                        </div>
                                        <div class="item-reviews" id="reviews-87" data-id="87">
                                            <div class="ava-user-rev" style="background-image: url('http://graph.facebook.com/1793976627292815/picture?type=square')"></div>
                                            <div class="reviews_main">
                                                <h4 class="name-user">
                <span class="avat-cm">
                    <div class="avatar-user" style="background-image: url('http://graph.facebook.com/1793976627292815/picture?type=square')"></div>
                </span>
                                                    <div class="box-user-rev">
                                                        <a href="http://facebook.com/1793976627292815" target="_blank">Minh Ngọc</a>
                                                        <div class="rating_user">
                                                            <i class="mcon-star active"></i><i class="mcon-star active"></i><i class="mcon-star active"></i><i class="mcon-star active"></i><i class="mcon-star"></i>                                            <span>2 năm trước</span>
                                                        </div>
                                                    </div>
                                                </h4>

                                                <p class="content-reviews"><span>qưer</span></p>
                                            </div>
                                            <div class="bottom-reviews">
                                                <a class="reply">Trả lời</a>
                                                <a class="more" onclick="getListSubReviews(87)" href="javascript:void(0)">Xem thêm 1 phản hồi</a>
                                            </div>

                                            <div class="sub-reviews " id="sub-reviews-87">
                                            </div>
                                        </div>
                                        <div class="item-reviews" id="reviews-67" data-id="67">
                                            <div class="ava-user-rev" style="background-image: url('http://graph.facebook.com/976183229204830/picture?type=square')"></div>
                                            <div class="reviews_main">
                                                <h4 class="name-user">
                <span class="avat-cm">
                    <div class="avatar-user" style="background-image: url('http://graph.facebook.com/976183229204830/picture?type=square')"></div>
                </span>
                                                    <div class="box-user-rev">
                                                        <a href="http://facebook.com/976183229204830" target="_blank">Vy Huấn</a>
                                                        <div class="rating_user">
                                                            <i class="mcon-star active"></i><i class="mcon-star active"></i><i class="mcon-star active"></i><i class="mcon-star active"></i><i class="mcon-star active"></i>                                            <span>2 năm trước</span>
                                                        </div>
                                                    </div>
                                                </h4>

                                                <p class="content-reviews"><span>Nước ép hỗn hợp Rau và trái cây tổng hợp Tipco có hương vị thơm ngon của những nguyên liệu tự nhiên được chọn lọc kỹ càng, sản phẩm giúp cơ thể bù đắp nước, bổ sung năng lượng, nhiều vitamin có lợi nhất là vitamin C, giúp xua tan cơn khát và cảm giác mệt mỏi</span></p>
                                            </div>
                                            <div class="bottom-reviews">
                                                <a class="reply">Trả lời</a>
                                                <a class="more" onclick="getListSubReviews(67)" href="javascript:void(0)">Xem thêm 3 phản hồi</a>
                                            </div>

                                            <div class="sub-reviews " id="sub-reviews-67">
                                            </div>
                                        </div>
                                        <div class="item-reviews" id="reviews-24" data-id="24">
                                            <div class="ava-user-rev" style="background-image: url('http://graph.facebook.com/1087212044751363/picture?type=square')"></div>
                                            <div class="reviews_main">
                                                <h4 class="name-user">
                <span class="avat-cm">
                    <div class="avatar-user" style="background-image: url('http://graph.facebook.com/1087212044751363/picture?type=square')"></div>
                </span>
                                                    <div class="box-user-rev">
                                                        <a href="http://facebook.com/1087212044751363" target="_blank">Nguyễn Thanh Linh</a>
                                                        <div class="rating_user">
                                                            <i class="mcon-star active"></i><i class="mcon-star active"></i><i class="mcon-star active"></i><i class="mcon-star"></i><i class="mcon-star"></i>                                            <span>2 năm trước</span>
                                                        </div>
                                                    </div>
                                                </h4>

                                                <p class="content-reviews"><span>hay</span></p>
                                            </div>
                                            <div class="bottom-reviews">
                                                <a class="reply">Trả lời</a>
                                                <a class="more" onclick="getListSubReviews(24)" href="javascript:void(0)">Xem thêm 1 phản hồi</a>
                                            </div>

                                            <div class="sub-reviews " id="sub-reviews-24">
                                            </div>
                                        </div>
                                        <div class="item-reviews" id="reviews-18" data-id="18">
                                            <div class="ava-user-rev" style="background-image: url('http://graph.facebook.com/1087212044751363/picture?type=square')"></div>
                                            <div class="reviews_main">
                                                <h4 class="name-user">
                <span class="avat-cm">
                    <div class="avatar-user" style="background-image: url('http://graph.facebook.com/1087212044751363/picture?type=square')"></div>
                </span>
                                                    <div class="box-user-rev">
                                                        <a href="http://facebook.com/1087212044751363" target="_blank">Nguyễn Thanh Linh</a>
                                                        <div class="rating_user">
                                                            <i class="mcon-star active"></i><i class="mcon-star active"></i><i class="mcon-star active"></i><i class="mcon-star active"></i><i class="mcon-star"></i>                                            <span>2 năm trước</span>
                                                        </div>
                                                    </div>
                                                </h4>

                                                <p class="content-reviews"><span>sane phẩm tốt</span></p>
                                            </div>
                                            <div class="bottom-reviews">
                                                <a class="reply">Trả lời</a>
                                            </div>

                                            <div class="sub-reviews sub-reviews-fix" id="sub-reviews-18">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!--   -->        </div>
                <div class="same-pro-right">
                    <div class="similar-product">
                        <h3>Sản phẩm tương tự</h3>
                        @foreach($pro_similar as $similar_item)
                        <div class="similar-product-item">
                            <a title="{{$similar_item->pro_name}}" href="{{$similar_item->pro_link}}">
                                <img alt="{{$similar_item->pro_name}}" class="img-pro-same" src="{{$similar_item->thumb}}">
                            </a>
                            <!--<img class="img-pro-hot-same" src="../assets/images/hot.gif"/>-->
                            <p class="price-same">
                                {{number_format($similar_item->pro_price)}} VNĐ                                                            </p>
                            <h4>
                                <a title="{{$similar_item->pro_name}}" href="{{$similar_item->pro_link}}">{{$similar_item->pro_name}}</a>
                            </h4>

                        </div>
                            @endforeach
                    </div>
                    <div class="lastProductView">
                    </div>
                </div>
            </div>


        </div>
    </section>
@endsection