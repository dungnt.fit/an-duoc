@extends('layouts/main')
@section('content')
    <section id="page-category" class="content">
        <div class="inner-content g1180">
            <div class="wrap-content w100 fl">
                <ul class="breadcrumbs">
                    {{--@foreach($cat_parent as $cat_par)--}}
                        {{--@foreach($cat_par as $item_par)--}}
                    <ol class="breadcrumb-page " itemscope="" itemtype="http://schema.org/BreadcrumbList">
                        <li class="item-breadcrumb" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" title="Trang chủ" href="/">
                                <span itemprop="name">Trang chủ</span>
                            </a>
                            <meta itemprop="position" content="1"></li>
                        @if(isset($cat_parent[$cat->cat_id]))
                            @foreach($cat_parent[$cat->cat_id] as $breadcrumb)
                        <li class="item-breadcrumb" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" title="{{$breadcrumb->cat_name}}" href="/{{$breadcrumb->cat_name}}">
                                <span itemprop="name">{{$breadcrumb->cat_name}}</span>
                            </a>
                            <meta itemprop="position" content="2">
                        </li>
                            @endforeach
                        @endif
                        <li class="item-breadcrumb" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" title="{{$cat->cat_name}}" href="/{{$cat->cat_rewrite}}">
                                <span itemprop="name">{{$cat->cat_name}}</span>
                            </a><meta itemprop="position" content="3">
                        </li>
                    </ol>
                        {{--@endforeach--}}
                    {{--@endforeach--}}
                </ul>
                <!--<link rel="stylesheet" href="/assets/css/sidebar.css?v=">-->
                <input type="checkbox" class="filter_category mcon-filter">
                <div class="sidebar w20 fl">
                    <div class="full-sidebar w100 fl">
                        <div id="box_cat" class="box-category-product w100 fl" data-rewrite="tpcn-giam-can" data-id="2">
                            <div class="title-box-category-product">
                                <h3> Danh Mục</h3>
                            </div>
                            <div class="content-box-sidebar">
                                <ul class="list-box-category-product">
                                    <li class="item-caegory-product active">
                                        <a title="{{$cat->cat_name}}" href="/{{$cat->cat_rewrite}}">
                                            <svg class="shopee-svg-icon icon-down-arrow-right-filled shopee-category-list__main-category__caret" viewBox="0 0 4 7" style="
                                display: inline-block;
                                width: 0.5em;
                                height: 0.5em;
                                fill: currentColor;
                                position: relative;
                                stroke: currentColor;
                                position: absolute;
                                left: -11px;
                                top: 13px;
                            ">
                                                <polygon points="4 3.5 0 0 0 7"></polygon>
                                            </svg>
                                            {{$cat->cat_name}}</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="box-filter-price w100 fl">
                            <div class="title-box-category-product">
                                <h3> Khoảng Giá</h3>
                            </div>
                            <div class="content-box-sidebar">
                                <div class="wrap-box-category-product w100 fl padding-5">
                                    <input type="hidden" class="pricce-products" data-current-max="1800000" data-current-min="0" data-min="0" data-max="1800000" data-step="5000">
                                    <input type="text" id="amount" class="filter_price_product" readonly="" style="border:0; color:#f6931f; font-weight:bold;">
                                    <div id="slider-range" class="ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content"><div class="ui-slider-range ui-corner-all ui-widget-header" style="left: 0%; width: 100%;"></div><span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default" style="left: 0%;"></span><span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default" style="left: 100%;"></span></div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>

                <div class="full-container w80 fr">
                    <div class="sort-by-options w100 fl">
                        <ul class="wrap-sort-by-options w100 fl">
                            <li class="item-sort-by-op options w20">
                                <a data-sort="pop" data-order="desc" class="item-data-sort">Phổ biến</a>
                            </li>
                            <li class="item-sort-by-op options  w20">
                                <a class="item-data-sort" data-sort="time" data-order="desc">Mới nhất </a>
                            </li>
                            <li class="item-sort-by-op options w20">
                                <a data-sort="sales" data-order="desc" class="item-data-sort">Bán chạy</a>
                            </li>
                            <li class="item-sort-by-op price options  w20">
                                <p>Giá</p>
                                <ul class="sub-sort-by-op">
                                    <li class="item-sort-by-op">
                                        <a class="item-data-sort" data-sort="price" data-order="asc">Giá: Thấp đến cao</a>
                                    </li>
                                    <li class="item-sort-by-op">
                                        <a class="item-data-sort" data-sort="price" data-order="desc">Giá: cao đến thấp</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="item-sort-by-op  w20">
                                <label for="shop-like">
                                    <input id="shop-like" type="checkbox">
                                    <span class="checkmark"></span>
                                    Shop yêu thích                            </label>
                            </li>
                        </ul>
                    </div>
                    <div class="wrap-container w100  fl">


                        <div class="box-lis-product w100 fl">

                            @foreach($pro as $pro_cat)
                                <?php
                                $a = (int)$pro_cat->pro_price;
                                $b = (int)$pro_cat->pro_old_price;
                                if($pro_cat->pro_old_price!=0){
                                    $promotion = 100-(($a/$b)*100);
                                }else
                                    $promotion=0;

                                ?>
                            <div class="item-product w20  padding-5 fl" data_pro_id="1">
                                <div class="wrap-item-product w100 fl">
                                    <div title="{{$pro_cat->pro_name}}" class="avatar-product w100 fl ">
                                        <a title="{{$pro_cat->pro_name}}" href="{{$pro_cat->pro_link}}">
                                            <img title="{{$pro_cat->pro_name}}" src="{{$pro_cat->thumb}}" alt="{{$pro_cat->pro_name}}">
                                        </a>
                                    </div>

                                    <div title="{{$pro_cat->pro_name}}" class="name-product w100 fl">
                                        <a title="{{$pro_cat->pro_name}}" href="{{$pro_cat->pro_link}}">
                                            {{$pro_cat->pro_name}}
                                        </a>
                                    </div>
                                    <div class="product_favorite">
                                        <div class="content_favorite">
                                            <span class="tick-svg-icon icon-tick"><i class="mcon-check"></i></span>
                                            Yêu thích                    </div>

                                    </div>
                                    @if($promotion!=0)
                                        <div class="product-sale">
                                            <p class=" number">
                                                <?php echo (int)$promotion . '%'?>
                                            </p>
                                            <p class="text">Giảm</p>
                                        </div>
                                    @endif
                                    <div class="price-shipping-product w100 fl">
                                        <div class="price-product">
                                            <del>₫{{number_format($pro_cat->pro_old_price)}}</del>
                                            <ins>₫{{number_format($pro_cat->pro_price)}}</ins>
                                        </div>
                                        <div class="shipping-svg-icon icon-free-shipping"></div>
                                    </div>
                                    <div class="rating-like">
                                        <div class="rating-like__wrap">
                                            <div class="like">
                                                <div class="shopee-svg-icon icon-like-2">
                                                    <i class="mcon-heart"></i>
                                                </div>
                                                <div class="number-like">1</div>
                                            </div>
                                            <div class="rating">
                                                <div class="shopee-rating-stars">
                                                    <div class="shopee-rating-stars__stars">

                                                        <div class="shopee-rating-stars__star-wrapper">
                                                            <div class="shopee-rating-stars__star-wrapper" style="position: absolute;z-index: 2;">
                                                                @for($i=0;$i < (int)$pro_cat->pro_rating;$i++)
                                                                    <div class="shopee-rating-stars__lit">
                                                                        <i class="mcon-star active" ></i>
                                                                    </div>
                                                                @endfor
                                                                @if(($pro_cat->pro_rating-(int)$pro_cat->pro_rating)!=0)
                                                                    <div class="shopee-rating-stars__lit">
                                                                        <i class="mcon-star active" style="position: absolute;width: {{($pro_cat->pro_rating-(int)$pro_cat->pro_rating)*100}}%;color: #ea0;z-index: 2;"></i>
                                                                    </div>
                                                                @endif
                                                            </div>
                                                            <div class="shopee-rating-stars__star-wrapper">
                                                                <div class="shopee-rating-stars__lit">
                                                                    <i class="mcon-star"></i>
                                                                </div>
                                                                <div class="shopee-rating-stars__lit">
                                                                    <i class="mcon-star"></i>
                                                                </div>
                                                                <div class="shopee-rating-stars__lit">
                                                                    <i class="mcon-star"></i>
                                                                </div>
                                                                <div class="shopee-rating-stars__lit">
                                                                    <i class="mcon-star"></i>
                                                                </div>
                                                                <div class="shopee-rating-stars__lit">
                                                                    <i class="mcon-star"></i>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="number-rating">({{$pro_cat->pro_rating}})</div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                                @endforeach
                        </div>
                        <div class="pagiantion-product-by-category w100 fl center">
                            <span class="active"><a title="1" href="tpcn-giam-can?page=1">1</a></span>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    @endsection