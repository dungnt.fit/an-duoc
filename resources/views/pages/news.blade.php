@extends('layouts.main')
@section('content')
<!-- Begin content -->
<section id="content">
    <div class="inner-content g1180">
        <div class="wrap-content w100 fl">


            <div class="clear-fix"></div>
            <div class="new_content">

                <ul class="breadcrumbs">
                    <ol class="breadcrumb-page " itemscope="" itemtype="http://schema.org/BreadcrumbList"><li class="item-breadcrumb" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><a itemprop="item" title="Trang chủ" href="http://dev.anduoc.vn"><span itemprop="name">Trang chủ</span></a><meta itemprop="position" content="1"></li> <li class="item-breadcrumb" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><a itemprop="item" title="Tin tức-sự kiện" href="http://dev.anduoc.vn/tin-tuc"><span itemprop="name">Tin tức-sự kiện</span></a><meta itemprop="position" content="2"></li></ol></ul>
                <div class="clear-fix">

                </div>
                <div class="new_top">
                    <div class="new_top-left">
                        <div class="content">
                            <a href="{{$news_hot->new_rewrite}}" title="{{$news_hot->new_title}}">
                                <img data-source="/1/assets/images/noimage.png" src="/assets/upload/full/{{$news_hot->new_picture}} " onerror="img_error(this)">
                            </a>
                            <h4 class="new-lead">
                                <a href="{{$news_hot->new_rewrite}}" title="{{$news_hot->new_title}}">
                                    {{$news_hot->new_title}}                 </a>
                            </h4>
                            <p>{{$news_hot->new_teaser}}</p>
                        </div>
                    </div>
                    <div class="new_top-right">
                        <ul>
                            @foreach($news as $news_item)
                            <li>
                                <div class="content">
                                    <div class="new_top-right-image">
                                        <img src="/assets/upload/full/{{$news_item->new_picture}}" alt="{{$news_item->new_title}}" title="{{$news_item->new_title}}" onerror="img_error(this)">
                                    </div>
                                    <div class="content-text">
                                        <h4>
                                            <a href="{{$news_item->new_rewrite}}-news{{$news_item->new_id}}.html" title="{{$news_item->new_title}}">
                                                {{$news_item->new_title}}</a>
                                        </h4>
                                        <p>{{$news_item->new_teaser}}</p>
                                    </div>

                                </div></li>
                            @endforeach


                        </ul>


                    </div>

                </div>


                <!--Begin backgroundMenu-->
                <div class="new-bottom">




                    <div class="new-category">
                        @foreach($cat_news as $cat_news_item)
                            <div class="new-category-item">
                                <h3>
                                    <a href="{{$cat_news_item->cat_rewrite}}" title="{{$cat_news_item->cat_name}}">{{$cat_news_item->cat_name}}</a>
                                </h3>
                                <div class="new-category-item-first-image">
                                    <a href="{{$cat_news_item->news[0]['new_rewrite']}}" title="{{$cat_news_item->cat_name}}">
                                        <img src="/assets/upload/full/{{$cat_news_item->news[0]['new_picture']}}" alt="" onerror="img_error(this)">
                                    </a>
                                </div>
                                <div class="new-category-item-first-text">
                                    <h4><a href="{{$cat_news_item->news[0]['new_rewrite']}}" title="{{$cat_news_item->news[0]['new_title']}}">{{$cat_news_item->news[0]['new_title']}}</a></h4>
                                    <p>{{$cat_news_item->news[0]['new_teaser']}}</p>
                                </div>

                                <ul>
                                    @foreach($cat_news_item->news as $key=>$item_news)
                                    <li>

                                        <div class="new-category-item-image">
                                            <a href="{{$item_news['new_rewrite']}}" title="{{$cat_news_item->cat_name}}">
                                                <img src="/assets/upload/full/{{$item_news['new_picture']}}" alt="" onerror="img_error(this)">
                                            </a>
                                        </div>
                                        <div class="new-category-item-title">

                                            <a href="{{$item_news['new_rewrite']}}" title="{{$item_news['new_title']}}">
                                                {{$item_news['new_title']}}</a>
                                        </div>
                                    </li>
                                        @endforeach
                                </ul>
                            </div>
                        @endforeach
                    </div>
                </div>


            </div>

            <!--Begin backgroundMenu-->


            <!--Begin content-index-->


            <div class="hot-product">

                @foreach($pro_hot as $hot_product)
                    <div class="product-hot-item">
                        <div class="pro-hot-item-image">
                            <img src="{{$hot_product->thumb}}" onerror="img_error(this)">
                        </div>
                        <div class="pro-hot-item-text">
                            <h4>
                                <a title="{{$hot_product->pro_name}}" href="{{$hot_product->pro_link}}">{{$hot_product->pro_name}}</a>
                            </h4>
                            <p class="price-same">
                                {{number_format($hot_product->pro_price)}}            </p>
                        </div>
                    </div>
                @endforeach


            </div>



        </div>
    </div>
    <!--End content-index-->
</section>
<!-- End content -->
@endsection
