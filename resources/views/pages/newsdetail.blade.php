@extends('layouts.main')
@section('content')
    <section id="content">
        <div class="inner-content g1180">
            <div class="wrap-content w100 fl">
                <div class=" content-detail">

                    <ul class="breadcrumbs">
                        <ol class="breadcrumb-page " itemscope="" itemtype="http://schema.org/BreadcrumbList"><li class="item-breadcrumb" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><a itemprop="item" title="Trang chủ" href="http://dev.anduoc.vn"><span itemprop="name">Trang chủ</span></a><meta itemprop="position" content="1"></li> <li class="item-breadcrumb" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><a itemprop="item" title="Tin tức-sự kiện" href="http://dev.anduoc.vn/tin-tuc"><span itemprop="name">Tin tức-sự kiện</span></a><meta itemprop="position" content="2"></li> <li class="item-breadcrumb" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><a itemprop="item" title="Sức khỏe" href="http://dev.anduoc.vn/suc-khoe-nc65"><span itemprop="name">Sức khỏe</span></a><meta itemprop="position" content="3"></li> <li class="item-breadcrumb" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><a itemprop="item" title="3 Lý do thuyết phục bạn nên tập bơi ngay bây giờ" href="http://dev.anduoc.vn/3-ly-do-thuyet-phuc-ban-nen-tap-boi-ngay-bay-gio.html-news181.html"><span itemprop="name">3 Lý do thuyết phục bạn nên tập bơi ngay bây giờ</span></a><meta itemprop="position" content="4"></li></ol></ul>
                    <div class="clear-fix"></div>

                    <div class="new-content">
                        <h1>3 Lý do thuyết phục bạn nên tập bơi ngay bây giờ</h1>
                        <p>
                            <i class="mcon-clock"></i>
                            10/03/2018 10:03:20        </p>
                        <div class="new-content-detail">
                            <p>
                                <strong>Có thể đối với bạn, việc tập thể dục trên những chiếc máy chạy bộ hay sử dụng tạ đã quá quen thuộc và nhàm chán. Nếu bạn đang tìm kiếm một phương pháp luyện tập giúp đốt cháy calo toàn cơ thể một cách hoàn hảo nhất thì hãy thử tập bơi xem nào!
                                    Bơi lội tốt cho người bị viêm khớp</strong>
                            </p>
                            <div class="short_desc">
                                <p style="box-sizing:border-box; margin:0px; padding:0px; outline:none; font-family:sans-serif; font-size:14px; text-align:center"><img alt="" src="http://cdn.nhanh.vn/cdn/store/16539/artCT/30482/2b.jpg" style="box-sizing: border-box; border: 0px; max-width: 100%; height: 375px; width: 500px;"></p>

                                <p style="box-sizing: border-box; margin: 0px; padding: 0px; outline: none; font-family: sans-serif; font-size: 14px;">Bơi lội là một bài tập thể dục aerobic tuyệt vời cho hầu hết những bệnh nhân viêm khớp. Hơn nữa, bơi lội có thể giúp làm giảm tải các khớp xương và giúp ngăn ngừa tổn thương. Nó cũng là một lựa chọn tốt cho bạn khi bị đau vùng dưới lưng. Bạn nhớ thảo luận với bác sĩ của bạn trước khi quyết định đi bơi nếu bạn bị đau khớp, thay khớp hay là những chấn thương gần đây bạn nhé.</p>

                                <p style="box-sizing: border-box; margin: 0px; padding: 0px; outline: none; font-family: sans-serif; font-size: 14px;"><strong style="box-sizing: border-box;">Bơi lội giúp điều trị bệnh tiểu đường</strong></p>

                                <p style="box-sizing:border-box; margin:0px; padding:0px; outline:none; font-family:sans-serif; font-size:14px; text-align:center"><img alt="" src="http://cdn.nhanh.vn/cdn/store/16539/artCT/30482/3a.jpg" style="box-sizing: border-box; border: 0px; max-width: 100%; height: 375px; width: 500px;"></p>

                                <p style="box-sizing: border-box; margin: 0px; padding: 0px; outline: none; font-family: sans-serif; font-size: 14px;">Nếu bạn bị tiểu đường, bơi lội có thể là một phần rất quan trọng trong kế hoạch điều trị bệnh của bạn. Nó sẽ giúp bạn tiêu thụ calo,&nbsp;<u style="box-sizing: border-box;"><em style="box-sizing: border-box;"><strong style="box-sizing: border-box;"><a href="http://khonggia.com/tpcn-vien-giam-can-new-perfect-ho-tro-giam-can-an-toan.-p4381443.html" style="box-sizing: border-box; outline: none; text-decoration-line: none; color: rgb(51, 51, 51);" target="_blank">giảm cân</a></strong></em></u>&nbsp;và giữ mức đường huyết trong kiểm soát. Nếu bạn có lượng cholesterol cao, bơi lội cũng sẽ giúp ích cho bạn. Bơi lội giúp bạn giảm nồng độ LDL (cholesterol xấu) và tăng nồng độ HDL (cholesterol tốt).</p>

                                <p style="box-sizing: border-box; margin: 0px; padding: 0px; outline: none; font-family: sans-serif; font-size: 14px;"><strong style="box-sizing: border-box;">Bơi lội giúp giảm cân hiệu quả</strong></p>

                                <p style="box-sizing:border-box; margin:0px; padding:0px; outline:none; font-family:sans-serif; font-size:14px; text-align:center"><img alt="" src="http://cdn.nhanh.vn/cdn/store/16539/artCT/30482/3b.jpg" style="box-sizing: border-box; border: 0px; max-width: 100%; height: 366px; width: 550px;"></p>

                                <p style="box-sizing: border-box; margin: 0px; padding: 0px; outline: none; font-family: sans-serif; font-size: 14px;">Những người luôn bơi với cường độ cao sẽ&nbsp; nâng cao nhịp tim của họ đồng thời đốt cháy một lượng calo đáng kể và giúp giảm cân. Điều quan trọng mà bạn cần đó là nỗ lực và cố gắng từng ngày.</p>

                                <p style="box-sizing: border-box; margin: 0px; padding: 0px; outline: none; font-family: sans-serif; font-size: 14px;">Ngoài ra, việc mặc áo tắm và xuất hiện trước mọi người trong bể bơi cũng phần nào động viên tinh thần bạn để có ý chí giảm thêm vài cân!</p>

                                <p style="box-sizing: border-box; margin: 0px; padding: 0px; outline: none; font-family: sans-serif; font-size: 14px;"><strong style="box-sizing: border-box;">Một số lưu ý khi bơi</strong></p>

                                <p style="box-sizing: border-box; margin: 0px; padding: 0px; outline: none; font-family: sans-serif; font-size: 14px;">- Khởi động trước khi bơi là một điều bắt buộc. Nếu không khởi động sẽ dẫn đến chuột rút nguy hiểm đến tính mạng.</p>

                                <p style="box-sizing: border-box; margin: 0px; padding: 0px; outline: none; font-family: sans-serif; font-size: 14px;">- Tương tự như các cách tập luyện&nbsp;<u style="box-sizing: border-box;"><em style="box-sizing: border-box;"><strong style="box-sizing: border-box;"><a href="http://khonggia.com/tpcn-vien-giam-can-new-perfect-ho-tro-giam-can-an-toan.-p4381443.html" style="box-sizing: border-box; outline: none; text-decoration-line: none; color: rgb(51, 51, 51);">giảm cân</a></strong></em></u>&nbsp;khác là không bơi khi đói hoặc sau khi ăn xong. Bạn nên ăn nhẹ trước lúc bơi 20 – 30 phút.</p>

                                <p style="box-sizing: border-box; margin: 0px; padding: 0px; outline: none; font-family: sans-serif; font-size: 14px;">- Không bơi trong tình trạng say xỉn.</p>

                                <p style="box-sizing: border-box; margin: 0px; padding: 0px; outline: none; font-family: sans-serif; font-size: 14px;">- Không bơi khi đang bị bệnh: cảm, sốt, cúm…</p>

                                <p style="box-sizing: border-box; margin: 0px; padding: 0px; outline: none; font-family: sans-serif; font-size: 14px;">- Không bơi ở những nơi không an toàn như ao hồ thiên nhiên, sông, suối… Chỉ bơi ở hồ bơi và có người theo dõi.</p>

                                <p style="box-sizing: border-box; margin: 0px; padding: 0px; outline: none; font-family: sans-serif; font-size: 14px;">- Để giảm cân nhanh cần bơi 30 – 45 phút/ lần, 2 – 3 lần/ tuần.</p>

                                <p style="box-sizing:border-box; margin:0px; padding:0px; outline:none; font-family:sans-serif; font-size:14px; text-align:center"><img alt="" src="http://cdn.nhanh.vn/cdn/store/16539/artCT/30482/4a.jpg" style="box-sizing: border-box; border: 0px; max-width: 100%; height: 312px; width: 500px;"></p>

                                <p style="box-sizing: border-box; margin: 0px; padding: 0px; outline: none; font-family: sans-serif; font-size: 14px;"><strong style="box-sizing: border-box;">Các động tác bơi lội phổ biến giúp giảm cân hiệu quả:</strong></p>

                                <ul style="box-sizing: border-box; margin: 0px; padding-right: 0px; padding-left: 0px; outline: none; font-family: sans-serif; font-size: 14px;">
                                    <li style="box-sizing: border-box; margin: 0px; padding: 0px; list-style: none; outline: none;">Bơi sải</li>
                                    <li style="box-sizing: border-box; margin: 0px; padding: 0px; list-style: none; outline: none;">Bơi bướm</li>
                                    <li style="box-sizing: border-box; margin: 0px; padding: 0px; list-style: none; outline: none;">Bơi ngửa</li>
                                    <li style="box-sizing: border-box; margin: 0px; padding: 0px; list-style: none; outline: none;">Bơi ếch</li>
                                </ul>

                                <p style="box-sizing: border-box; margin: 0px; padding: 0px; outline: none; font-family: sans-serif; font-size: 14px;">Ngoài ra, bạn cũng nên kết hợp sử dụng thực phẩm chức năng giảm cân New Perfect song song với việc bơi lội để có kết quả nhanh hơn bạn nhé. Hãy tham khảo thông tin sản phẩm tại link:<u style="box-sizing: border-box;"><em style="box-sizing: border-box;"><strong style="box-sizing: border-box;"><a href="http://khonggia.com/tpcn-vien-giam-can-new-perfect-ho-tro-giam-can-an-toan.-p4381443.html" style="box-sizing: border-box; outline: none; text-decoration-line: none; color: rgb(51, 51, 51);">http://khonggia.com/tpcn-vien-giam-can-new-perfect-ho-tro-giam-can-an-toan.-p4381443.html</a></strong></em></u></p>

                                <p style="box-sizing: border-box; margin: 0px; padding: 0px; outline: none; font-family: sans-serif; font-size: 14px; text-align: center;"><a href="http://khonggia.com/tpcn-vien-giam-can-new-perfect-ho-tro-giam-can-an-toan.-p4381443.html" style="box-sizing: border-box; outline: none; text-decoration-line: none; color: rgb(51, 51, 51);"><u style="box-sizing: border-box;"><em style="box-sizing: border-box;"><strong style="box-sizing: border-box;"><img alt="" src="http://cdn.nhanh.vn/cdn/store/16539/artCT/30482/new_perfect_(ba380ded_707a_45d9_8e9d_5c2ce81b986a).png" style="box-sizing: border-box; border: 0px; max-width: 100%; height: 500px; width: 500px;"></strong></em></u></a></p>

                            </div>
                            <div class="social-network-box">

                                <iframe src="https://www.facebook.com/plugins/like.php?href=http%3A%2F%2Fdev.anduoc.vn/3-ly-do-thuyet-phuc-ban-nen-tap-boi-ngay-bay-gio.html-news181.html&amp;width=112&amp;layout=button&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=65&amp;appId=914235252058138" width="112" height="65" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true">

                                </iframe>
                                <div class="fb-comments fb_iframe_widget fb_iframe_widget_fluid_desktop" data-href="https://www.facebook.com/hoanglinh5.12.96" data-numposts="5" width="100%" fb-xfbml-state="rendered" fb-iframe-plugin-query="app_id=914235252058138&amp;container_width=651&amp;height=100&amp;href=https%3A%2F%2Fwww.facebook.com%2Fhoanglinh5.12.96&amp;locale=vi_VN&amp;numposts=5&amp;sdk=joey&amp;version=v2.12" style="width: 100%;"><span style="vertical-align: bottom; width: 100%; height: 484px;"><iframe name="f27a2dacd72bb1c" width="1000px" height="100px" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" allow="encrypted-media" title="fb:comments Facebook Social Plugin" src="https://www.facebook.com/v2.12/plugins/comments.php?app_id=914235252058138&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter%2Fr%2Fd_vbiawPdxB.js%3Fversion%3D44%23cb%3Df68fd0db3c718%26domain%3Ddev.anduoc.vn%26origin%3Dhttp%253A%252F%252Fdev.anduoc.vn%252Ff21d5f4d0efb958%26relation%3Dparent.parent&amp;container_width=651&amp;height=100&amp;href=https%3A%2F%2Fwww.facebook.com%2Fhoanglinh5.12.96&amp;locale=vi_VN&amp;numposts=5&amp;sdk=joey&amp;version=v2.12" style="border: none; visibility: visible; width: 100%; height: 484px;" class=""></iframe></span></div>

                            </div>


                        </div>


                    </div>








                </div>

                <div class="hot-product-new">
                    <div class="product-hot-col">
                        <hgroup class="title_box_category_product">
                            <h4><a title="xem nhiều nhất" href=":;" class="first">Sản phẩm hot</a></h4>
                        </hgroup>



                        <div class="product-hot-item">

                            <div class="pro-hot-item-image">
                                <img data-src="/upload/thumb/200/2018/01/hjz1517394280.jpg" src="/upload/thumb/200/2018/01/hjz1517394280.jpg" onerror="img_error(this)">
                            </div>

                            <div class="pro-hot-item-text">


                                <h4>
                                    <a title="Sữa Glucerna hương Vani Abbott lon 400g" href="/sua-glucerna-huong-vani-abbott-lon-400g-p786.html">Sữa Glucerna hương Vani Abbott lon 400g</a>
                                </h4>



                                <p class="price-same">
                                    320,000 VNĐ            </p>


                            </div>


                        </div>



                        <div class="product-hot-item">

                            <div class="pro-hot-item-image">
                                <img data-src="/upload/thumb/200/2018/01/eqc1517394279.jpg" src="/upload/thumb/200/2018/01/eqc1517394279.jpg" onerror="img_error(this)">
                            </div>

                            <div class="pro-hot-item-text">


                                <h4>
                                    <a title="Sữa Nước Ensure Hương Vani 237 ml" href="/sua-nuoc-ensure-huong-vani-237-ml-p785.html">Sữa Nước Ensure Hương Vani 237 ml</a>
                                </h4>



                                <p class="price-same">
                                    37,000 VNĐ            </p>


                            </div>


                        </div>



                        <div class="product-hot-item">

                            <div class="pro-hot-item-image">
                                <img data-src="/upload/thumb/200/2018/01/wgx1517394279.jpg" src="/upload/thumb/200/2018/01/wgx1517394279.jpg" onerror="img_error(this)">
                            </div>

                            <div class="pro-hot-item-text">


                                <h4>
                                    <a title="Sữa Bột Ensure Gold Hương Vani Hộp 400g" href="/sua-bot-ensure-gold-huong-vani-hop-400g-p784.html">Sữa Bột Ensure Gold Hương Vani Hộp 400g</a>
                                </h4>



                                <p class="price-same">
                                    349,000 VNĐ            </p>


                            </div>


                        </div>



                        <div class="product-hot-item">

                            <div class="pro-hot-item-image">
                                <img data-src="/upload/thumb/200/2018/01/kbq1517394278.jpg" src="/upload/thumb/200/2018/01/kbq1517394278.jpg" onerror="img_error(this)">
                            </div>

                            <div class="pro-hot-item-text">


                                <h4>
                                    <a title="MILO nước uống liền lốc 4x180ml" href="/milo-nuoc-uong-lien-loc-4x180ml-p783.html">MILO nước uống liền lốc 4x180ml</a>
                                </h4>



                                <p class="price-same">
                                    26,000 VNĐ            </p>


                            </div>


                        </div>



                        <div class="product-hot-item">

                            <div class="pro-hot-item-image">
                                <img data-src="/upload/thumb/200/2018/01/hsl1517394278.jpg" src="/upload/thumb/200/2018/01/hsl1517394278.jpg" onerror="img_error(this)">
                            </div>

                            <div class="pro-hot-item-text">


                                <h4>
                                    <a title="Matcha Tâm Thái An hộp 100g
" href="/matcha-tam-thai-an-hop-100g-p782.html">Matcha Tâm Thái An hộp 100g
                                    </a>
                                </h4>



                                <p class="price-same">
                                    99,000 VNĐ            </p>


                            </div>


                        </div>



                        <div class="product-hot-item">

                            <div class="pro-hot-item-image">
                                <img data-src="/upload/thumb/200/2018/01/vre1517394277.jpg" src="/upload/thumb/200/2018/01/vre1517394277.jpg" onerror="img_error(this)">
                            </div>

                            <div class="pro-hot-item-text">


                                <h4>
                                    <a title="Cà phê sáng tạo 4 Trung Nguyên" href="/ca-phe-sang-tao-4-trung-nguyen-p781.html">Cà phê sáng tạo 4 Trung Nguyên</a>
                                </h4>



                                <p class="price-same">
                                    75,000 VNĐ            </p>


                            </div>


                        </div>



                        <div class="product-hot-item">

                            <div class="pro-hot-item-image">
                                <img data-src="/upload/thumb/200/2018/01/irp1517394277.jpg" src="/upload/thumb/200/2018/01/irp1517394277.jpg" onerror="img_error(this)">
                            </div>

                            <div class="pro-hot-item-text">


                                <h4>
                                    <a title="Nước Dừa Tipco 1L
" href="/nuoc-dua-tipco-1l-p780.html">Nước Dừa Tipco 1L
                                    </a>
                                </h4>



                                <p class="price-same">
                                    50,000 VNĐ            </p>


                            </div>


                        </div>



                        <div class="product-hot-item">

                            <div class="pro-hot-item-image">
                                <img data-src="/upload/thumb/200/2018/01/qnv1517394276.jpg" src="/upload/thumb/200/2018/01/qnv1517394276.jpg" onerror="img_error(this)">
                            </div>

                            <div class="pro-hot-item-text">


                                <h4>
                                    <a title="Nước suối Aquafina 355ml
" href="/nuoc-suoi-aquafina-355ml-p779.html">Nước suối Aquafina 355ml
                                    </a>
                                </h4>



                                <p class="price-same">
                                    4,000 VNĐ            </p>


                            </div>


                        </div>



                        <div class="product-hot-item">

                            <div class="pro-hot-item-image">
                                <img data-src="/upload/thumb/200/2018/01/ddr1517394276.jpg" src="/upload/thumb/200/2018/01/ddr1517394276.jpg" onerror="img_error(this)">
                            </div>

                            <div class="pro-hot-item-text">


                                <h4>
                                    <a title="Nước ép Dứa Tipco hộp 1 L
" href="/nuoc-ep-dua-tipco-hop-1-l-p778.html">Nước ép Dứa Tipco hộp 1 L
                                    </a>
                                </h4>



                                <p class="price-same">
                                    50,000 VNĐ            </p>


                            </div>


                        </div>



                        <div class="product-hot-item">

                            <div class="pro-hot-item-image">
                                <img data-src="/upload/thumb/200/2018/01/rne1517394275.jpg" src="/upload/thumb/200/2018/01/rne1517394275.jpg" onerror="img_error(this)">
                            </div>

                            <div class="pro-hot-item-text">


                                <h4>
                                    <a title="Đông Trùng Hạ Thảo AT hộp 20g" href="/dong-trung-ha-thao-at-hop-20g-p777.html">Đông Trùng Hạ Thảo AT hộp 20g</a>
                                </h4>



                                <p class="price-same">
                                    1,880,000 VNĐ            </p>


                            </div>


                        </div>



                        <div class="product-hot-item">

                            <div class="pro-hot-item-image">
                                <img data-src="/upload/thumb/200/2018/01/cyx1517394275.jpg" src="/upload/thumb/200/2018/01/cyx1517394275.jpg" onerror="img_error(this)">
                            </div>

                            <div class="pro-hot-item-text">


                                <h4>
                                    <a title="Nước ép cam Tipco hộp 1L
" href="/nuoc-ep-cam-tipco-hop-1l-p776.html">Nước ép cam Tipco hộp 1L
                                    </a>
                                </h4>



                                <p class="price-same">
                                    50,000 VNĐ            </p>


                            </div>


                        </div>



                        <div class="product-hot-item">

                            <div class="pro-hot-item-image">
                                <img data-src="/upload/thumb/200/2018/01/cww1517394274.jpg" src="/upload/thumb/200/2018/01/cww1517394274.jpg" onerror="img_error(this)">
                            </div>

                            <div class="pro-hot-item-text">


                                <h4>
                                    <a title="Cà phê hòa tan G7 3 in 1 x 18 gói
" href="/ca-phe-hoa-tan-g7-3-in-1-x-18-goi-p775.html">Cà phê hòa tan G7 3 in 1 x 18 gói
                                    </a>
                                </h4>



                                <p class="price-same">
                                    43,000 VNĐ            </p>


                            </div>


                        </div>



                        <div class="product-hot-item">

                            <div class="pro-hot-item-image">
                                <img data-src="/upload/thumb/200/2018/01/qog1517394273.jpg" src="/upload/thumb/200/2018/01/qog1517394273.jpg" onerror="img_error(this)">
                            </div>

                            <div class="pro-hot-item-text">


                                <h4>
                                    <a title="Cà phê sáng tạo số 1 Trung Nguyên
" href="/ca-phe-sang-tao-so-1-trung-nguyen-p774.html">Cà phê sáng tạo số 1 Trung Nguyên
                                    </a>
                                </h4>



                                <p class="price-same">
                                    47,000 VNĐ            </p>


                            </div>


                        </div>



                        <div class="product-hot-item">

                            <div class="pro-hot-item-image">
                                <img data-src="/upload/thumb/200/2018/01/ffi1517394273.jpg" src="/upload/thumb/200/2018/01/ffi1517394273.jpg" onerror="img_error(this)">
                            </div>

                            <div class="pro-hot-item-text">


                                <h4>
                                    <a title="Nước ép Táo và trái cây tổng hợp Tipco hộp 1 L
" href="/nuoc-ep-tao-va-trai-cay-tong-hop-tipco-hop-1-l-p773.html">Nước ép Táo và trái cây tổng hợp Tipco hộp 1 L
                                    </a>
                                </h4>



                                <p class="price-same">
                                    42,000 VNĐ            </p>


                            </div>


                        </div>
                    </div>
                    <div class="new-same-cate">
                        <hgroup class="title_box_category">
                            <h4><a title="xem nhiều nhất" href=":;" class="first">Tin tức cùng chuyên mục</a></h4>
                        </hgroup>
                        <div class="new-item">
                            <p>
                            </p><p>
                                <a href="/3-meo-giam-can-cuc-chuan-de-chao-don-tet-news182.html" title="3 mẹo giảm cân cực chuẩn để chào đón Tết">
                                    3 mẹo giảm cân cực chuẩn để chào đón Tết            </a>
                            </p>
                            <p></p>
                        </div>
                        <div class="new-item">
                            <p>
                            </p><p>
                                <a href="/ban-nen-an-gi-truoc-khi-tap-gym-news183.html" title="Bạn nên ăn gì trước khi tập gym?">
                                    Bạn nên ăn gì trước khi tập gym?            </a>
                            </p>
                            <p></p>
                        </div>
                        <div class="new-item">
                            <p>
                            </p><p>
                                <a href="/ban-co-dang-giam-can-dung-cach-news184.html" title="Bạn có đang giảm cân đúng cách?">
                                    Bạn có đang giảm cân đúng cách?            </a>
                            </p>
                            <p></p>
                        </div>
                        <div class="new-item">
                            <p>
                            </p><p>
                                <a href="/ban-da-biet-cach-giam-can-bang-trung-ga.html-news185.html" title="Bạn đã biết cách giảm cân bằng trứng gà?">
                                    Bạn đã biết cách giảm cân bằng trứng gà?            </a>
                            </p>
                            <p></p>
                        </div>
                        <div class="new-item">
                            <p>
                            </p><p>
                                <a href="/thoi-quen-dung-giay-ve-sinh-co-that-su-tot-news186.html" title="Thói quen dùng giấy vệ sinh có thật sự tốt?">
                                    Thói quen dùng giấy vệ sinh có thật sự tốt?            </a>
                            </p>
                            <p></p>
                        </div>
                        <div class="new-item">
                            <p>
                            </p><p>
                                <a href="/vi-sao-co-the-can-omega-3-6-9-news187.html" title="Vì sao cơ thể cần omega 3-6-9?">
                                    Vì sao cơ thể cần omega 3-6-9?            </a>
                            </p>
                            <p></p>
                        </div>
                        <div class="new-item">
                            <p>
                            </p><p>
                                <a href="/cat-giam-duong-co-phai-la-bien-phap-tranh-benh-tieu-duong-tot-nhat-news188.html" title="Cắt giảm đường có phải là biện pháp tránh bệnh tiểu đường tốt nhất?">
                                    Cắt giảm đường có phải là biện pháp tránh bệnh tiểu đường tốt nhất?            </a>
                            </p>
                            <p></p>
                        </div>
                        <div class="new-item">
                            <p>
                            </p><p>
                                <a href="/nhung-nguoi-beo-phi-nen-biet-cac-tac-hai-nay.html-news189.html" title="Những người béo phì nên biết các tác hại này">
                                    Những người béo phì nên biết các tác hại này            </a>
                            </p>
                            <p></p>
                        </div>
                        <div class="new-item">
                            <p>
                            </p><p>
                                <a href="/tet-den-uong-nuoc-trai-cay-nao-la-tot.html-news190.html" title="Tết đến, uống nước trái cây nào là tốt?">
                                    Tết đến, uống nước trái cây nào là tốt?            </a>
                            </p>
                            <p></p>
                        </div>

                    </div>


                </div>


            </div>
        </div>
        <!--End content-index-->
    </section>
    @endsection