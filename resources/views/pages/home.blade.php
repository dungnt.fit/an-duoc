<?php
//    foreach ($cat_parent[0] as $pro)
//    dd($pro)
?>
@extends('/layouts/main')
@section('content')
    <section id="content">

        <div id="backgroundMenu">

            <div id="homeSlide" class="g1180">
                <div class="jsSlide">
                    <div class="carousel-slide">
                        <ul class="layer-slide">
                            <li class="slide-item">
                                <a href="#">
                                    <img data-src="/assets/upload/full/tnr1520267570.jpg" src="/assets/images/default.jpg" onerror="img_error(this)" alt="Slide home 1">
                                </a>
                            </li>
                            <li class="slide-item">
                                <a href="#">
                                    <img data-src="/assets/upload/full/kvg1520267584.jpg" src="/assets/images/default.jpg" onerror="img_error(this)" alt="Slide home 2">
                                </a>
                            </li>
                            <li class="slide-item">
                                <a href="#">
                                    <img data-src="/assets/upload/full/tvw1517560502.jpg" src="/assets/images/default.jpg" onerror="img_error(this)" alt="Slide home 3">
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div id="bnRight">
                        <div><a href="#"><img src="/assets/upload/full/ceq1520267298.jpg" onerror="img_error(this)" alt="Banner home 1"></a></div>
                        <div><a href="#"><img src="/assets/upload/full/bnk1520267305.jpg" onerror="img_error(this)" alt="Banner home 2"></a></div>
                    </div>
                </div>
            </div>
            <div id="hommPolicy">
                <p>
                    <i class="si"></i> Giao hàng toàn quốc        </p>
                <p>
                    <i class="si"></i> Thanh toán khi nhận hàng        </p>
                <p>
                    <i class="si"></i> Đổi trả trong 10 ngày        </p>
                <p>
                    <a href="/tin-tuc" style="color: #333"> <i class="si"></i> Tin tức - Sự kiện</a>
                </p>

            </div>


        </div>

        <div id="content-index">
            <div id="slide-product" class="g1180">
                <div id="slide-product-title">
                    <p></p>
                    <p></p>
                    <span>Sản phẩm mua nhiều</span>
                    <div class="list-filter">
                        <!--<a href="" class="">Sản phẩm bán chạy <i class="label_icon label_hot">hot</i></a>-->
                        <a href="javascipt:void(0)" onclick="filterAction('new', this)" class="">Mới nhất</a>
                        <a href="javascipt:void(0)" onclick="filterAction('hot', this)" class="active">Hot nhất</a>
                        <!--<a href="" class="">Xem nhiều</a>
                        <a href="" class="">Giảm giá</a>-->
                    </div>
                </div>
                <div id="carousel-wraper">
                    <span class="loading-content display-slide"></span>
                    <div id="slider-play">
                        <div id="hot" class="hidden-slide active">
                            <ul class="product-slider">
                                <div class="slicktest">
                                    @foreach($products_total as $pro_total)

                                        <?php
                                            $a = (int)$pro_total->pro_price;
                                            $b = (int)$pro_total->pro_old_price;
                                            if($pro_total->pro_old_price!=0){
                                                $promotion = 100-(($a/$b)*100);
                                            }else
                                                $promotion=0;

                                        ?>
                                    <div class="item-product w20  padding-5 fl" >
                                        <div class="wrap-item-product w100 fl">
                                            <div title="{{$pro_total->pro_name}}" class="avatar-product w100 fl ">
                                                <a title="{{$pro_total->pro_name}}" href="{{$pro_total->pro_link}}">
                                                    <img title="{{$pro_total->pro_name}}" data-src="{{$pro_total->thumb}}" src="{{$pro_total->thumb}}" alt="{{$pro_total->pro_name}}">
                                                </a>
                                            </div>
                                            <div title="{{$pro_total->pro_name}}" class="name-product w100 fl">
                                                <a title="{{$pro_total->pro_name}}" href="{{$pro_total->pro_link}}">
                                                    {{$pro_total->pro_name}}                                </a>
                                            </div>
                                            <div class="product_favorite">
                                                <div class="content_favorite">
                                                    <span class="tick-svg-icon icon-tick" ><i class="mcon-check"></i></span>
                                                    Yêu thích                                </div>

                                            </div>
                                            @if($promotion!=0)
                                            <div class="product-sale">
                                                <p class=" number">
                                                    <?php echo (int)$promotion . '%'?>
                                                </p>
                                                <p class="text">Giảm</p>
                                            </div>
                                            @endif

                                            <div class="price-shipping-product w100 fl">
                                                <div class="price-product">
                                                    <del>₫{{number_format($pro_total->pro_old_price)}}</del>
                                                    <ins>₫{{number_format($pro_total->pro_price)}}</ins>
                                                </div>
                                                <div class="shipping-svg-icon icon-free-shipping" ></div>
                                            </div>
                                            <div class="rating-like">
                                                <div class="rating-like__wrap">
                                                    <div class="like">
                                                        <div class="shopee-svg-icon icon-like-2">
                                                            <i class="mcon-heart"></i>
                                                        </div>
                                                        <div class="number-like">10</div>
                                                    </div>
                                                    <div class="rating">
                                                        <div class="shopee-rating-stars">
                                                            <div class="shopee-rating-stars__stars">

                                                                <div class="shopee-rating-stars__star-wrapper">
                                                                    <div class="shopee-rating-stars__star-wrapper" style="position: absolute;z-index: 2;">
                                                                        @for($i=0;$i < (int)$pro_total->pro_rating;$i++)
                                                                            <div class="shopee-rating-stars__lit">
                                                                                <i class="mcon-star active" ></i>
                                                                            </div>
                                                                        @endfor
                                                                        @if(($pro_total->pro_rating-(int)$pro_total->pro_rating)!=0)
                                                                                <div class="shopee-rating-stars__lit">
                                                                                    <i class="mcon-star active" style="position: absolute;width: {{($pro_total->pro_rating-(int)$pro_total->pro_rating)*100}}%;color: #ea0;z-index: 2;"></i>
                                                                                </div>
                                                                        @endif
                                                                    </div>
                                                                    <div class="shopee-rating-stars__star-wrapper">
                                                                        <div class="shopee-rating-stars__lit">
                                                                            <i class="mcon-star"></i>
                                                                        </div>
                                                                        <div class="shopee-rating-stars__lit">
                                                                            <i class="mcon-star"></i>
                                                                        </div>
                                                                        <div class="shopee-rating-stars__lit">
                                                                            <i class="mcon-star"></i>
                                                                        </div>
                                                                        <div class="shopee-rating-stars__lit">
                                                                            <i class="mcon-star"></i>
                                                                        </div>
                                                                        <div class="shopee-rating-stars__lit">
                                                                            <i class="mcon-star"></i>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="number-rating">({{$pro_total->pro_rating}})</div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </ul>
                        </div>

                        <div id="new" class="hidden-slide">

                        </div>
                        <div class="clear-fix"></div>
                    </div>
                </div>
            </div>

            <div id="showCate" class="g1180">
                <?php $dem = 1?>
                @if(isset($cat_parent[0]))
                    @foreach($cat_parent[0] as $cat_item)
                        <div id="cate-item-1" class="show-cate-item">
                            <div class="title-cate">
                                <h2>
                                    <a title="{{$cat_item->cat_name}}"
                                       href="/{{$cat_item->cat_rewrite}}">
                                        <span><i><?php echo $dem++ ?></i></span>
                                        {{$cat_item->cat_name}}<i class="mcon-caret-right"></i>
                                    </a>
                                </h2>
                                <div class="review-buy">
                                    <a title="{{$cat_item->cat_name}}" href="/{{$cat_item->cat_rewrite}}"><i class="mcon-shopping-cart-2"></i><span
                                                class="sp-l1">Mua nhiều</span></a>
                                    <span></span>
                                    <a title="{{$cat_item->cat_name}}" href="/{{$cat_item->cat_rewrite}}"><i class="mcon-eye"></i><span class="sp-l1">Xem nhiều</span></a>
                                </div>
                                <div class="list-item-cate">
                                    @if(isset($cat_parent[$cat_item->cat_id]))
                                        @foreach($cat_parent[$cat_item->cat_id] as $item_child)
                                            <a title="{{$item_child->cat_name}}"
                                           href="/{{$item_child->cat_rewrite}}">
                                            <i class="mcon-angle-right"></i>{{$item_child->cat_name}}</a>
                                        @endforeach
                                    @endif
                            </div>
                            </div>
                            <div class="olw-pro-cate">
                                <ul>
                                    <div class="owl-wrapper">
                                        <li class="olw-item-pro"><a title="{{$cat_item->cat_name}}"
                                                                    href="/{{$cat_item->cat_rewrite}}">
                                                <img data-src="{{$cat_item->thumb}}" src="{{$cat_item->thumb}}" alt="{{$cat_item->cat_name}}"
                                                     onerror="img_error(this)">

                                            </a>
                                        </li>
                                    </div>
                                </ul>
                                <div class="clear-fix"></div>

                                <!-- <div class="dots">
                                                    <span data-id="" class="dot "></span>
                                        </div>-->
                            </div>
                            <div class="list-product">
                                <?php $dem1 = 0 ;?>
                                    @foreach($pro_cat as $key => $pro_cat_1)
                                        <?php
                                        $a = (int)$pro_cat_1->pro_price;
                                        $b = (int)$pro_cat_1->pro_old_price;
                                        if($pro_cat_1->pro_old_price!=0){
                                            $promotion = 100-(($a/$b)*100);
                                        }else
                                            $promotion=0;

                                        ?>
                                    @if($pro_cat_1->pro_cat_root_id == $cat_item->cat_id)
                                        @if($dem1 == 6)
                                            @continue
                                        @endif
                                        <?php $dem1++;?>
                                        <div class="item-product w30  padding-5 fl" >
                                            <div class="wrap-item-product w100 fl">
                                            <div title="{{$pro_cat_1->pro_name}}" class="avatar-product w100 fl ">
                                                <a title="{{$pro_cat_1->pro_name}}" href="{{$pro_cat_1->pro_link}}">
                                                    <img title="{{$pro_cat_1->pro_name}}" onerror="img_error(this)" data-src="{{$pro_cat_1->thumb}}" src="{{$pro_cat_1->thumb}}" alt="{{$pro_cat_1->pro_name}}">
                                                </a>
                                            </div>
                                            <div title="{{$pro_cat_1->pro_name}}" class="name-product w100 fl">
                                                <a title="{{$pro_cat_1->pro_name}}" href="{{$pro_cat_1->pro_link}}">
                                                    {{$pro_cat_1->pro_name}}</a>
                                            </div>
                                            <div class="product_favorite">
                                                <div class="content_favorite">
                                                    <span class="tick-svg-icon icon-tick" ><i class="mcon-check"></i></span>
                                                    Yêu thích                                    </div>

                                            </div>
                                            @if($promotion!=0)
                                                <div class="product-sale">
                                                    <p class=" number">
                                                        <?php echo (int)$promotion . '%'?>
                                                    </p>
                                                    <p class="text">Giảm</p>
                                                </div>
                                            @endif
                                            <div class="price-shipping-product w100 fl">
                                                <div class="price-product">
                                                    <del title="₫{{number_format($pro_total->pro_old_price)}}">
                                                        ₫{{number_format($pro_total->pro_old_price)}}                                           </del>
                                                    <ins>₫{{number_format($pro_total->pro_price)}}</ins>
                                                </div>
                                                <div class="shipping-svg-icon icon-free-shipping" ></div>
                                            </div>
                                            <div class="rating-like">
                                                <div class="rating-like__wrap">
                                            <div class="like">
                                                <div class="shopee-svg-icon icon-like-2">
                                                    <i class="mcon-heart"></i>
                                                </div>
                                                <div class="number-like">10</div>
                                            </div>
                                            <div class="rating">
                                                <div class="shopee-rating-stars">
                                                    <div class="shopee-rating-stars__stars">

                                                        <div class="shopee-rating-stars__star-wrapper">
                                                            <div class="shopee-rating-stars__star-wrapper" style="position: absolute;z-index: 2;">
                                                                @for($i=0;$i < (int)$pro_cat_1->pro_rating;$i++)
                                                                    <div class="shopee-rating-stars__lit">
                                                                        <i class="mcon-star active" ></i>
                                                                    </div>
                                                                @endfor
                                                                @if(($pro_cat_1->pro_rating-(int)$pro_cat_1->pro_rating)!=0)
                                                                    <div class="shopee-rating-stars__lit">
                                                                        <i class="mcon-star active" style="position: absolute;width: {{($pro_total->pro_rating-(int)$pro_total->pro_rating)*100}}%;color: #ea0;z-index: 2;"></i>
                                                                    </div>
                                                                @endif
                                                            </div>
                                                            <div class="shopee-rating-stars__star-wrapper">
                                                                <div class="shopee-rating-stars__lit">
                                                                    <i class="mcon-star"></i>
                                                                </div>
                                                                <div class="shopee-rating-stars__lit">
                                                                    <i class="mcon-star"></i>
                                                                </div>
                                                                <div class="shopee-rating-stars__lit">
                                                                    <i class="mcon-star"></i>
                                                                </div>
                                                                <div class="shopee-rating-stars__lit">
                                                                    <i class="mcon-star"></i>
                                                                </div>
                                                                <div class="shopee-rating-stars__lit">
                                                                    <i class="mcon-star"></i>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="number-rating">({{$pro_cat_1->pro_rating}})</div>
                                            </div>
                                        </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                    @endforeach



                        <a class="view-all" href="/thuc-pham-chuc-nang"
                           title="Thực phẩm chức năng">Xem tất cả</a>
                    </div>
                </div>
                    @endforeach
                @endif


            </div>

        </div>
    </section>
@endsection