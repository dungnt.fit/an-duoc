/**
 * Created by Iminha on 14/07/2018.
 */
var error_consult = '';
var emailWarning = '<p class="emailErr">Email không hợp lệ</p>';
var nameWarning = '<p class="nameErr">Tên không hợp lệ</p>';
var phoneWarning = '<p class="phoneErr">Số điện thoại không hợp lệ</p>';
var filledWarning = '<p class="filledErr">Vui lòng điền đầy đủ thông tin</p>';

var consultNameValidated = false;
var consultPhoneValidated = false;
var consultEmailValidated = false;

var reviewNameValidated = false;
var reviewPhoneValidated = false;
var reviewEmailValidated = false;

var consultInputValidated = false;
var reviewInputValidated = false;

var phone11NumPrefix = ['0162', '0163', '0164', '0165', '0166', '0167', '0168', '0169', '0120', '0121', '0122', '0126', '0128', '0123', '0124', '0125', '0127', '0129'];
var phone10NumPrefix = ['086', '096', '097', '098', '090', '093', '091', '094'];

$(document).ready(function(){
    console.log(reviewInputValidated);
    $('#consult_form').keyup(function(){
        var consultNameSelector = $(this).find("#con_name").val();
        var consultEmailSelector = $(this).find("#con_email").val();
        var consultPhoneSelector = $(this).find("#con_phone").val();

        if($.trim(consultNameSelector).length > 0 && $.trim(consultPhoneSelector).length > 0 && $.trim(consultEmailSelector).length > 0){
            if($.trim($("#con_email").val()).indexOf('@') > -1){
                if($('.emailErr').length){
                    $('.emailErr').remove();
                }
                $('#con_email').css('border', '1px solid green');
                consultEmailValidated = true;
            }
            if($.trim($("#con_name").val()).length >= 2 && $.isNumeric($.trim(consultNameSelector)) != true){
                if($('.nameErr').length){
                    $('.nameErr').remove();
                }
                $('#con_name').css('border', '1px solid green');
                consultNameValidated = true;
            }
            if($.trim($("#con_phone").val()).length > 9 && $.trim($("#con_phone").val()).length < 12){
                if(($.trim(consultPhoneSelector).length == 11
                    && phone11NumPrefix.indexOf($.trim(consultPhoneSelector).substr(0, 4)) == -1)
                    ||
                    ($.trim(consultPhoneSelector).length == 10
                        && phone10NumPrefix.indexOf($.trim(consultPhoneSelector).substr(0, 3)) == -1)){
                    if(!$('.phoneErr').length){
                        $(this).find('.error_consult').append(phoneWarning);
                        $('#con_phone').css('border', '1px solid red');
                    }
                    consultPhoneValidated = false;
                }
                if(($.trim(consultPhoneSelector).length == 11
                    && phone11NumPrefix.indexOf($.trim(consultPhoneSelector).substr(0, 4)) > -1)
                    ||
                    ($.trim(consultPhoneSelector).length == 10
                        && phone10NumPrefix.indexOf($.trim(consultPhoneSelector).substr(0, 3)) > -1)){
                    if($('.phoneErr').length){
                        $('.phoneErr').remove();
                    }
                    $('#con_phone').css('border', '1px solid green');
                    consultPhoneValidated = true;
                }
            }
            // ----------------------------------------------------------------------------
            if(consultEmailValidated && consultNameValidated && consultPhoneValidated){
                consultInputValidated = true;
            }
            if(!consultEmailValidated || !consultNameValidated || !consultPhoneValidated){
                consultInputValidated = false;
            }
            if(consultInputValidated){
                //$('#captcha_modal').attr('rel','').attr('href', '#');
                if($('.filledErr').length){
                    $('.filledErr').remove();
                }
            }
            if(!consultInputValidated){
                $('#captcha_modal').removeAttr('rel').removeAttr('href');
            }
            // -------------------------------------------------------------------------------
            if($.trim($(this).find("#con_email").val()).indexOf('@') == -1){
                if(!$('.emailErr').length){
                    $(this).find('.error_consult').append(emailWarning);
                    $('#con_email').css('border', '1px solid red');
                }
                consultEmailValidated = false;
            }
            if($.trim(consultNameSelector).length < 2 || $.isNumeric($.trim(consultNameSelector)) == true){
                if(!$('.nameErr').length){
                    $(this).find('.error_consult').append(nameWarning);
                    $('#con_name').css('border', '1px solid red');
                }
                consultNameValidated = false
            }
            if($.trim(consultPhoneSelector).length <= 9 || $.trim($("#con_phone").val()).length >= 12){
                if(!$('.phoneErr').length){
                    $(this).find('.error_consult').append(phoneWarning);
                    $('#con_phone').css('border', '1px solid red');
                }
                consultPhoneValidated = false;
            }
        }
        else{
            if(error_consult.indexOf(filledWarning) == -1){
                error_consult += filledWarning;
            }
        }
    });

    $('#captcha_review').click(function (event) {
        $('#brg_body').removeClass('focus');
        $(this).closest('#form_reviews').removeClass('focus');
    });

    $('#form_reviews').keyup(function(){
        var reviewNameSelector = $(this).find("#review_name").val();
        var reviewEmailSelector = $(this).find("#review_email").val();
        var reviewPhoneSelector = $(this).find("#review_phone").val();

        if($.trim(reviewNameSelector).length > 0 && $.trim(reviewPhoneSelector).length > 0 && $.trim(reviewEmailSelector).length > 0){
            if($.trim($("#review_email").val()).indexOf('@') != -1){
                if($('.emailErr').length){
                    $('.emailErr').remove();
                }
                $('#review_email').css('border', '1px solid green');
                reviewEmailValidated = true;
            }
            if($.trim($("#review_name").val()).length >= 2 && $.isNumeric($.trim(reviewNameSelector)) != true){
                if($('.nameErr').length){
                    $('.nameErr').remove();
                }
                $('#review_name').css('border', '1px solid green');
                reviewNameValidated = true;
            }
            if($.trim($("#review_phone").val()).length > 9 && $.trim($("#review_phone").val()).length < 12){
                if(($.trim(reviewPhoneSelector).length == 11
                    && phone11NumPrefix.indexOf($.trim(reviewPhoneSelector).substr(0, 4)) == -1)
                    ||
                    ($.trim(reviewPhoneSelector).length == 10
                        && phone10NumPrefix.indexOf($.trim(reviewPhoneSelector).substr(0, 3)) == -1)){
                    if(!$('.phoneErr').length){
                        $(this).find('.error_consult').append(phoneWarning);
                        $('#review_phone').css('border', '1px solid red');
                    }
                    reviewPhoneValidated = false;
                }
                if(($.trim(reviewPhoneSelector).length == 11
                    && phone11NumPrefix.indexOf($.trim(reviewPhoneSelector).substr(0, 4)) > -1)
                    ||
                    ($.trim(reviewPhoneSelector).length == 10
                        && phone10NumPrefix.indexOf($.trim(reviewPhoneSelector).substr(0, 3)) > -1)){
                    if($('.phoneErr').length){
                        $('.phoneErr').remove();
                    }
                    $('#review_phone').css('border', '1px solid green');
                    reviewPhoneValidated = true;
                }
            }
            //------------------------------------------------------
            if(reviewEmailValidated && reviewNameValidated && reviewPhoneValidated && $.trim($('#comment').val()).length > 0){
                reviewInputValidated = true;
            }
            if(!reviewEmailValidated || !reviewNameValidated || !reviewPhoneValidated || $.trim($('#comment').val()).length == 0){
                reviewInputValidated = false;
            }
            if(reviewInputValidated){
                //$('#captcha_review').attr('rel','modal:open').attr('href', '#ex2');
                if($('.filledErr').length){
                    $('.filledErr').remove();
                }
            }
            if(!reviewInputValidated){
                $('#captcha_review').removeAttr('rel').removeAttr('href');
            }
            //--------------------------------------------------------
            if($.trim(reviewEmailSelector).indexOf('@') == -1){
                if(!$('.emailErr').length){
                    $(this).find('.error_reviews').append(emailWarning);
                    $('#review_email').css('border', '1px solid red');
                }
                reviewEmailValidated = false;
            }
            if($.trim(reviewNameSelector).length < 2 || $.isNumeric($.trim(reviewNameSelector)) == true){
                if(!$('.nameErr').length){
                    $(this).find('.error_reviews').append(nameWarning);
                    $('#review_name').css('border', '1px solid red');
                }
                reviewNameValidated = false;
            }
            if($.trim(reviewPhoneSelector).length < 9 || $.trim($("#review_phone").val()).length >= 12){
                if(!$('.phoneErr').length){
                    $(this).find('.error_reviews').append(phoneWarning);
                    $('#review_phone').css('border', '1px solid red');
                }
                reviewPhoneValidated = false;
            }
        }
        else{
            if(error_consult.indexOf(filledWarning) == -1){
                error_consult += filledWarning;
            }
        }
    });
});

$(document).on('submit','#consult_form', function (event) {
    if(!consultInputValidated){
        event.preventDefault();
        if(!$('.filledErr').length) {
            $(this).find('.error_consult').append(filledWarning);
        }
    }
    if(consultInputValidated && $(this).find("#token").val().length == 0 && $(this).find("[name=spam_status]").val() == 1){
        event.preventDefault();
        // fade transition
        $('#ex1').modal({
            fadeDuration: 250,
            fadeDelay: 0.80
        });
        $("#consult_form").find(".error_consult").text("Bạn chưa xác minh mình không phải bot");
    }
    else{}
});

$(document).on('submit','#form_reviews', function (event) {
    if(!reviewInputValidated){
        event.preventDefault();
        if(!$('.filledErr').length) {
            $(this).find('.error_reviews').append(filledWarning);
        }
        console.log(2);
    }

    if(reviewInputValidated && $(this).find("#_token").val().length == 0 && $(this).find("[name=spam_status]").val() == 1){
        event.preventDefault();
        // fade transition
        $('#ex2').modal({
            fadeDuration: 250,
            fadeDelay: 0.80
        });
    }
    else{}
});

var onloadCallback = function() {
    captchaConsultWidget = grecaptcha.render('captcha_consult', {
        'sitekey': '6LeSWmQUAAAAAEGoiu33tTLWgAYXvN0Je6Niceqo',
        'theme': 'light',
    });

    captchaReviewWidget = grecaptcha.render('captcha_reviews', {
        'sitekey': '6LeSWmQUAAAAAEGoiu33tTLWgAYXvN0Je6Niceqo',
        'theme': 'light',
    });
};

function consultCallBack(){
    $('#captcha_modal').removeAttr('rel').removeAttr('href');
    var response = grecaptcha.getResponse(captchaConsultWidget);
    $('#token').attr("value",response);
    $("#consult_form").find(".error_reviews").text("");
    setTimeout($.modal.close, 1000);
}

function reviewCallback(){
    $('#captcha_review').removeAttr('rel').removeAttr('href');
    var _response = grecaptcha.getResponse(captchaReviewWidget);
    $('#_token').attr("value",_response);
    $("#form_reviews").find(".error_reviews").text("");
    setTimeout($.modal.close, 1000);
}


